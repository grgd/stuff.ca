var STAR_SIZE = 50.0,
	NEAR = 1.0,
	FAR = 10.0;

glMatrix.setMatrixArrayType(Float32Array);

var Space = (function () {
	var stars = [],
		N = 20,
		playerX = 0,
		playerY = 0;

	for(var i=0; i<N; i++) {
		//stars.push(vec3.fromValues(Math.random(), Math.random(), Math.random() * FAR));
		stars.push(vec4.fromValues(i / N, i / N,  NEAR * 1.001 + i * (FAR - NEAR) / N, 1.0));
		console.log(stars[i]);
	}

	return {
		stars: stars
	}
})();

var Display = (function () {

	var canvas = document.createElement('canvas'),
		width = window.innerWidth,
		height = window.innerHeight,
		aspect = 1.0,
		fov = 1.0,
		near = NEAR,
		far = FAR,
		viewMatrix = mat4.create(),
		projectionMatrix = mat4.create(),
		shades = [],
		pi = 2 * Math.PI;

	function resize() {
		width = window.innerWidth;
		height = window.innerHeight;
		aspect = width * 1.0 / height;

		mat4.perspective(projectionMatrix, fov, aspect, near, far);

		viewMatrix = mat4.create();
		var tm = mat4.create();
		mat4.translate(viewMatrix, tm, vec4.fromValues(0.0, 0.0, 0, 0));		
		//mat4.frustum(projectionMatrix, -aspect, aspect, -1, 1, 0.1, 100.0);
		//mat4.mul(projectionMatrix, projectionMatrix, tm);

		canvas.width = width;
		canvas.height = height;
	}

	function render() {
		var ctx = canvas.getContext('2d');

		ctx.fillStyle = '#000';
		ctx.fillRect(0, 0, width, height);

		var s = Space.stars,
			v = vec4.create(),
			z = 0;
		for(var i=s.length-1; i>=0; i--) {
			vec4.transformMat4(v, s[i], viewMatrix);
			vec4.transformMat4(v, v, projectionMatrix);

			
			z = (FAR + NEAR + v[2]) / (FAR - NEAR);
			if(z < 0) z = 0;
			if(z > 0.999) z = 0.999;
			console.log(v);
			vec4.scale(v, v, 1.0 / Math.abs(v[3]));
			console.log(v);

			v[0] += 0.5;
			v[1] += 0.5;
			ctx.fillStyle = shades[(z * 256) | 0];
			ctx.beginPath();
			ctx.arc(v[0] * width, v[1] * height, (z + 1) * 10, 0, pi);
			ctx.fill();
			console.log(z);
		}
	}

	for(var i=0; i<256; i++) {
		var c = i.toString(16);
		if(i < 16)
			c = '0' + c;
		shades.push('#' + c + c + c);
	}
	document.getElementById('container').appendChild(canvas);	
	resize();

	return {
		resize: resize,
		render: render
	}
})();

console.log("Render!");
Display.render();