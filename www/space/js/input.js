
var Input = (function () {
	function detectMobile() { 
		if( navigator.userAgent.match(/Android/i)
			|| navigator.userAgent.match(/webOS/i)
			|| navigator.userAgent.match(/iPhone/i)
			|| navigator.userAgent.match(/iPad/i)
			|| navigator.userAgent.match(/iPod/i)
			|| navigator.userAgent.match(/BlackBerry/i)
			|| navigator.userAgent.match(/Windows Phone/i)
		){
			return true;
		}
		else {
			return false;
		}
	}

	var isMobile = detectMobile();

	var x = WIDTH * 0.5,
		y = HEIGHT * 0.5,
		px = x,
		py = y,
		activeTouch = -1,
		scale = 0.5,
		type = isMobile ? "relative" : "absolute";

	function clamp() {
		var dx = x - 0.5,
			dy = y - 0.5,
			l = Math.sqrt(dx * dx + dy * dy);
		if(l > 0.45) {
			dx = dx * 0.45 / l;
			dy = dy * 0.45 / l;
			x = dx + 0.5;
			y = dy + 0.5;
		}
	}

	function clampDelta(dx, dy) {
		var l = Math.sqrt(dx * dx + dy * dy);
		if(l > 0.12) {
			dx = dx * 0.12 / l;
			dy = dy * 0.12 / l;
		}
		x += dx;
		y += dy;
	}

	function touchStartHandler(event) {
		event.preventDefault();
		var t = event.changedTouches;
		if(t.length == 0)
			return;
		activeTouch = t[0].identifier;
		px = t[0].pageX;
		py = t[0].pageY;
	}

	function touchMoveHandler(event) {
		event.preventDefault();
		var t = event.changedTouches;
		if(t.length == 0)
			return;
		var minSize = Math.min(Display.width(), Display.height()),
			dx = 0.0,
			dy = 0.0;
		for(var i=0; i<t.length; i++) {
			if(t[i].identifier == activeTouch) {
				dx = (t[i].pageX - px) * scale / minSize;
				dy = (t[i].pageY - py) * scale / minSize;
				clampDelta(dx, dy);
				clamp();
				px = t[i].pageX;
				py = t[i].pageY;
			}
		}
	}

	function msTouchStartHandler(event) {
		activeTouch = event.pointerId;
		px = event.pageX;
		py = event.pageY;
	}

	function msTouchMoveHandler(event) {
		event.preventDefault();
		var minSize = Math.min(Display.width(), Display.height()),
			dx = 0.0,
			dy = 0.0;
		//if(event.pointerId == activeTouch) {
			dx = (event.pageX - px) * scale / minSize;
			dy = (event.pageY - py) * scale / minSize;
			clampDelta(dx, dy);
			clamp();
			px = event.pageX;
			py = event.pageY;
		//}
	}

	function mouseMoveHandler(event) {
		x = event.pageX / Display.width();
		y = event.pageY / Display.height();
		clamp();
	}

	if(isMobile) {
		if(document.mspointerdown) {
			Display.canvas().addEventListener("mspointerdown", msTouchStartHandler);
			Display.canvas().addEventListener("mspointermove", msTouchMoveHandler);
		} else {
			Display.canvas().addEventListener("touchstart", touchStartHandler);
			Display.canvas().addEventListener("touchmove", touchMoveHandler);
		}
	} else {
		Display.canvas().addEventListener("mousemove", mouseMoveHandler);
	}

	return {
		x: function() { return x; },
		y: function() { return y; },
		mobile: function() { return isMobile; }
	}
})();