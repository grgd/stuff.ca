
var Star = function(x, y, z) {
	this.x = x || 0;
	this.y = y || 0;
	this.z = z || 0;
	this.r = 1.0; // temporary value
}

var Stars = (function () {
	var stars = [];

	var generators = [],
		currentNumber = -1,
		nextNumber = -1,
		currentGenerator = null,
		nextGenerator = null;

	function addStars(amount) {
		for(var i=0; i<amount; i++) {
			stars.push( new Star(0, 0, FAR) ); 
		}
	}

	function randomExcept(l, e) {
		var n = Math.random() * (l - 1) | 0;
		if(n == e)
			n = l - 1;
		return n;
	}

	function init(amount) {
		var interval = FAR / amount;

		//changeGenerator();
		currentNumber = 0;
		currentGenerator = generators[0];
		currentGenerator.init();
		nextNumber = randomExcept(generators.length, 0);
		nextGenerator = generators[nextNumber];
		nextGenerator.init();

		for(var i=0; i<amount; i++) {
			var s = new Star();
			currentGenerator.next(s);
			s.z = (i + 1) * interval;
			stars.push(s);
		}
	}

	function changeGenerator() {
		currentNumber = nextNumber;
		currentGenerator = nextGenerator;
		//currentGenerator = generators[currentNumber];
		//currentGenerator.init();
		nextNumber = randomExcept(generators.length, nextNumber);
		nextGenerator = generators[nextNumber];
		nextGenerator.init();
	}

	function step(speed, dt) {
		var p = 0, l = 0;
		for(var i=0; i<stars.length; i++) {
			if(stars[i].z < -NEAR) {
				p = currentGenerator.duration - currentGenerator.progress;
				l = nextGenerator.duration * 0.1;
				if(p < l) {
					Math.random() * l > p ? nextGenerator.next(stars[i]) : currentGenerator.next(stars[i]);
				} else
					currentGenerator.next(stars[i]);
			} else {
				stars[i].z -= speed * dt;
			}
			if(currentGenerator.complete())
				changeGenerator();
		}
		stars.sort(function(a, b) { return b.z - a.z; });
	}

	return {
		stars: stars,
		generators: generators,
		init: init,
		step: step
	};
})();