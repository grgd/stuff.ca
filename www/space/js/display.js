
var Display = (function () {

	var canvas = document.createElement('canvas'),
		width = window.innerWidth,
		height = window.innerHeight,
		vpSize = Math.min(width, height),
		aspect = 1.0,
		fov = 2.0,
		shades = [],
		cacheSize = (STAR_COUNT * 0.9) | 0,
		cachedImgSize = 32,
		cache = [],
		pi = 2 * Math.PI;

	function resize() {
		width = window.innerWidth;
		height = window.innerHeight;
		aspect = width * 1.0 / height;
		vpSize = Math.min(width, height);

		canvas.width = width;
		canvas.height = height;
	}

	function render() {
		var ctx = canvas.getContext('2d');
		ctx.imageSmoothingEnabled = false;
		ctx.msImageSmoothingEnabled = false;
		ctx.fillStyle = '#000';
		ctx.fillRect(0, 0, width, height);

		var stars = Stars.stars,
			cacheMax = stars.length - cacheSize,
			sx = 0.0, sy = 0.0,
			px = 0.0, py = 0.0,
			pos = App.pos,
			baseSize = (vpSize * STAR_SIZE),
			vsize = vpSize / VP_SIZE,
			ox = width / vsize / 2.0,
			oy = height / vsize / 2.0,
			near = NEAR,
			far = FAR,
			size = 0.0,
			iz = 0.0,
			nz = 0.0;

		for(var i=0; i<stars.length; i++) {
			var s = stars[i];
			sx = s.x - pos.x;
			sy = s.y - pos.y;
			if(s.z > near && s.z < far) {
				nz = (near + far - s.z) / far;
				nz = Math.pow(nz, 2);
				iz = 1.0 / (s.z * fov);
				size = baseSize * iz;
				RollerCoaster.offset(s);
				px = (sx * iz + s.oX + ox) * vsize;
				py = (sy * iz + s.oY + oy) * vsize;

				if(i < cacheSize) {
					ctx.drawImage(cache[(nz * 255) | 0],
							px - size,
							py - size,
							size * 2,
							size * 2);
				} else {
					ctx.fillStyle = shades[(nz * 255) | 0];
					ctx.beginPath();
					ctx.arc( px,
							 py,
							 size | 0,
							 0,
							 Math.PI * 2);
					ctx.fill();
				}
			}
		}
	}

	function testRender() {
		var ctx = canvas.getContext('2d');

		ctx.fillStyle = '#000';
		ctx.fillRect(0, 0, width, height);

		ctx.fillStyle = "#F00";
		ctx.fillRect(width * 0.1, height * 0.1, width * 0.8, height * 0.8);
	}

	for(var i=0; i<256; i++) {
		var c = i.toString(16);
		if(i < 16)
			c = '0' + c;
		shades.push('#' + c + c + c);
	}
	for(var i=0; i<256; i++) {
		var c = document.createElement('canvas');
		c.width = cachedImgSize;
		c.height = cachedImgSize;
		var ct = c.getContext("2d");
		ct.fillStyle = shades[i];
		ct.beginPath();
		ct.arc(cachedImgSize / 2, cachedImgSize / 2, cachedImgSize / 2, 0, Math.PI * 2);
		ct.fill();
		cache.push(c);
	}
	document.getElementById('container').appendChild(canvas);	
	resize();

	return {
		resize: resize,
		render: render,
		testRender: testRender,
		width: function() { return width; },
		height: function() { return height; },
		canvas: function() { return canvas; }
	}
})();