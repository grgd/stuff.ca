
var App = (function () {
	var starCount = Input.mobile() ? STAR_COUNT / 2 | 0 : STAR_COUNT;

	var playerPos = { x: 0.5, y: 0.5, z: 0.0 };

	var fps = 60,
		lastTime = -1,
		maxTime = 100;

	var t = 0.0;
	function testStep(now) {
		var newTime = (new Date()).getTime(),
			dt = Math.min(newTime - lastTime, maxTime);
		requestAnimationFrame(testStep);

		t += dt * 0.001;
		playerPos.x = Input.x();//0.5 + Math.cos(t) / 4;
		playerPos.y = Input.y();//0.5 + Math.sin(t) / 4;
		playerPos.z += 0.002 * dt;

		RollerCoaster.calcZeroOffset();
		Stars.step(0.002, dt);
		Display.render();

		lastTime = newTime;
		fps++;
	}

	function resetFPS() {
		document.title = fps + ' fps';
		fps = 0;
	}

	function init() {
		var resized = 0;
		window.addEventListener("resize", function() {
			resized ++;
			setTimeout(function() {
				resized --;
				if(resized == 0) {
					Display.resize();
					Display.render();
				}
			}, 90);
		});

		Stars.init(starCount);

		Display.render();

		lastTime = (new Date()).getTime();
		requestAnimationFrame(testStep);
		setInterval(resetFPS, 1000);
	}

	return {
		init: init,
		pos: playerPos,
		starCount: function() { return starCount; }
	}
})();

App.init();