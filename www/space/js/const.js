var FAR = 5.0,
	NEAR = 0.01,
	WIDTH = 1.0,
	HEIGHT = 1.0,
	STAR_COUNT = 400,
	STAR_SIZE = 0.04,
	VP_SIZE = 0.4; // viewport size (smaller size)
