
var StarGenerator = function() {
	this.baseDuration = 15;
	this.duration = 15; // stars
	this.progress = 0;

	this.next = function(star) {
		this.progress ++;
	}

	this.init = function() {
		this.progress = 0;
	}

	this.complete = function() {
		return this.progress == this.duration;
	}
}

var RollerCoaster = (function() {
	var offsetX = 0.0,
		offsetY = 0.0,
		angle = 0.0,
		amp = 0.4,

		maxAmp = 0.4,
		ampFreq = 0.05,
		angleFreq = 0.005;

	function getNoise(t) {
		var n = Perlin.get1d(t);
		return n;
	}

	function calcZeroOffset() {
		var pz = App.pos.z;
		angle = (getNoise(pz * angleFreq) - 0.5) * Math.PI * 2;
		amp = (getNoise(pz * ampFreq) - 0.5) * maxAmp;
		offsetX = Math.cos(angle) * amp;
		offsetY = Math.sin(angle) * amp;
	}

	function calcOffset(star) {
		var pz = App.pos.z + star.z;
		angle = (getNoise(pz * angleFreq) - 0.5) * Math.PI * 2;
		amp = (getNoise(pz * ampFreq) - 0.5) * maxAmp;
		star.oX = Math.cos(angle) * amp - offsetX,
		star.oY = Math.sin(angle) * amp - offsetY;
	}

	return {
		calcZeroOffset: calcZeroOffset,
		offset: calcOffset
	}
})();

function addGenerator(generator) {
	var g = generator;
	for(var i in generator) {
		if(generator.hasOwnProperty(i))
			g[i] = generator[i];
	}
	if(g.baseDuration == undefined)
		g.baseDuration = 150;
	g.duration = g.baseDuration;
	g.progress = 0;

	g.next = function(star) {
		this.generate(star);
		star.z += FAR;
		this.progress ++;
	}

	g.init = function() {
		this.progress = 0;
		this.start();
	}

	g.complete = function() {
		return this.progress >= this.duration;
	}

	Stars.generators.push(g);
}

/*addGenerator({
	name: "Angular",

	generate: function(star) {
		var a = [[0,0],[1,0],[1,1],[0,1]];
		star.x = a[this.progress % 4][0];
		star.y = a[this.progress % 4][1];
		star.z = FAR;
	},

	start: function() {
		this.duration = 30;//this.baseDuration + rnd_snd() * this.baseDuration * 0.2;	
	}
});*/

addGenerator({
	name: "Intro",

	baseDuration: 1500,

	generate: function(star) {
		var t = 2 * Math.PI * Math.random(),
			u = Math.random() + Math.random(),
			r = u > 1 ? 2 - u : u;
		r += 0.8 * (1.0 - this.progress / this.duration);
		r *= r;
		star.x = r * Math.cos(t) * 0.5 + 0.5;
		star.y = r * Math.sin(t) * 0.5 + 0.5;
	},

	start: function() {
		this.duration = this.baseDuration;
	}
});

addGenerator({
	name: "Basic",

	baseDuration: 1000,

	generate: function(star) {
		var t = 2 * Math.PI * Math.random(),
			u = Math.random() + Math.random(),
			r = u > 1 ? 2 - u : u;
		star.x = r * Math.cos(t) * 0.5 + 0.5;
		star.y = r * Math.sin(t) * 0.5 + 0.5;
		//star.z = FAR;
	},

	start: function() {
		this.duration = this.baseDuration + rnd_snd() * this.baseDuration * 0.2;	
	}
});

addGenerator({
	name: "Spiral",

	baseDuration: 400,
	branches: 1,
	phi: 0,

	generate: function(star) {
		var a = this.phi + this.progress * 0.2 + (this.progress % this.branches) * (Math.PI * 2) / this.branches,
			amp = (Math.cos(this.progress * 0.041)  + 1.3) / 2.5;
		star.x = 0.5 + Math.sin(a) * amp;
		star.y = 0.5 + Math.cos(a) * amp;
	},

	start: function() {
		this.duration = this.baseDuration + rnd_snd() * this.baseDuration * 0.1;
		this.branches = Math.floor(Math.random() * 4) + 1;
		this.phi = Math.random() * Math.PI;
	}
});

addGenerator({
	name: "Central",

	baseDuration: 900,

	generate: function(star) {
		var a = Math.random() * Math.PI * 2,
			r = Math.random() * 0.5;
		//r = Math.sqrt(r);
		star.x = 0.5 + Math.cos(a) * r;
		star.y = 0.5 + Math.sin(a) * r;
	},

	start: function() {
		this.duration = this.baseDuration + rnd_snd() * this.baseDuration * 0.2;
	}
});

addGenerator({
	name: "Double Spiral",

	baseDuration: 1000,
	dir: 1,

	generate: function(star) {
		var a1 = this.progress * 0.02 * this.dir,
			a2 = this.progress * 0.26 * this.dir,
			r1 = 0.21,
			r2 = 0.19;
		star.x = 0.5 + Math.cos(a1) * r1 + Math.cos(a2) * r2;
		star.y = 0.5 + Math.sin(a1) * r1 + Math.sin(a2) * r2;
	},

	start: function() {
		this.duration = this.baseDuration + rnd_snd() * this.baseDuration * 0.1;
		this.dir = Math.random() > 0.5 ? 1 : -1;
	}
});

addGenerator({
	name: "Sweeper",

	baseDuration: 900,
	dir: 1,
	width: 11,

	generate: function(star) {
		var c = this.progress / this.width | 0,
			a = c * this.dir * 0.22,
			r = (this.progress % this.width) / this.width - 0.5;//(this.progress % this.width) * 0.4 / this.width + 0.05;
		//r = Math.sqrt(Math.abs(r)) * (r > 0 ? 1 : -1) * 0.8;
		star.x = 0.5 + Math.cos(a) * r;
		star.y = 0.5 + Math.sin(a) * r;
	},

	start: function() {
		this.dir = Math.random() > 0.5 ? 1 : -1;
		this.duration = this.baseDuration + rnd_snd() * this.baseDuration * 0.1;
	}
});