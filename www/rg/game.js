(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

var dirs = require('./dirs.js');

var Creatures = {};

var Base = function () {
}

Base.prototype.init = function(options) {
	this.x = options.x || 0;
	this.y = options.y || 0;
	this.char = '@';
};

Base.prototype.step = function(dir) {
	if(map.canWalk(this.x + dirs[dir].x, this.y + dirs[dir].y)) {
		this.x += dirs[dir].x;
		this.y += dirs[dir].y;
	}
};

Base.prototype.spawn = function() {
	while(!map.canWalk(this.x, this.y)) {
		this.x = map.width * Math.random() | 0;
		this.y = map.height * Math.random() | 0;
	}
}

var Human = function(options) {
	this.init(options);
	this.char = '@';
}

Human.prototype = new Base();

Creatures.Human = Human;

module.exports = Creatures;

var map = require('./map.js');
},{"./dirs.js":2,"./map.js":5}],2:[function(require,module,exports){

var dirs = [
	{ x: 1, y: 0},
	{ x: 0, y: 1},
	{ x: -1, y: 0},
	{ x: 0, y: -1},
	{ x: 1, y: 1},
	{ x: 1, y: -1},
	{ x: -1, y: -1},
	{ x: -1, y: 1}
];

module.exports = dirs;
},{}],3:[function(require,module,exports){

var objectCells = {
	'wall': {
		char: '#',
		fg: '#FFF',
		bg: '#333'
	},
	'floor': {
		char: '.',
		fg: '#aaa',
		bg: '#000'
	},
	'nothing': {
		char: '0',
		fg: '#000',
		bg: '#000'
	},
	'human': {
		char: '@',
		fg: '#fff'
	}
};

var Display = {};

Display.init = function init (width, height) {
	this.width = width;
	this.height = height;

	this.offsetX = 0;
	this.offsetY = 0;

	this.data = [];
	for(var i=0; i<this.width; i++) {
		this.data.push([]);
		for(var j=0; j<this.height; j++) {
			this.data[i].push({
				char: '.',
				fg: '#FFF',
				bg: '#000'
			});
		}
	}

	this.element = document.getElementById('display');
	for(var j=0; j<this.height; j++) {
		var row = document.createElement('div');
		row.className = 'display-row';
		for(var i=0; i<this.width; i++) {
			var cell = document.createElement('span');
			row.appendChild(cell);
			this.data[i][j].cell = cell;
		}
		this.element.appendChild(row);
	}
}

Display.clear = function(char, fg, bg) {
	for(var i=0; i<this.width; i++) {
		for(var j=0; j<this.height; j++) {
			this.data[i][j].char = char;
			this.data[i][j].fg = fg;
			this.data[i][j].bg = bg;
		}
	}
}

Display.render = function() {
	var c;
	for(var i=0; i<this.width; i++) {
		for(var j=0; j<this.height; j++) {
			c = this.data[i][j];
			c.cell.style.backgroundColor = c.bg;
			c.cell.style.color = c.fg;
			c.cell.innerText = c.char;
		}
	}
}

Display.setCell = function(x, y, object) {
	if(x < 0 || y < 0 || x >= this.width || y >= this.height)
		return;
	var template = objectCells[object];
	var cell = this.data[x][y];
	cell.char = template.char || cell.char;
	cell.fg = template.fg || cell.fg;
	cell.bg = template.bg || cell.bg;
}

module.exports = Display;

},{}],4:[function(require,module,exports){

var display = require('./display.js'),
	map = require('./map.js'),
	creatures = require('./creatures.js');

var player;

var keyDelay = 80;
var keyFlag = true;

function drawFrame() {
	var ox = player.x - display.width / 2 | 0;
	var oy = player.y - display.height / 2 | 0;
	for(var i=0; i<display.width; i++) {
		for(var j=0; j<display.height; j++) {
			var cell = map.get(ox + i, oy + j);
			if(cell === undefined)
				console.log(ox + i, oy + j);
			var type;
			switch(cell.type) {
				case map.types.WALL:
					type = 'wall';
					break;
				case map.types.FLOOR:
					type = 'floor';
					break;
				case map.types.NONE:
					type = 'nothing';
					break;
			}
			display.setCell(i, j, type);
		}
	}
	display.setCell(player.x - ox, player.y - oy, 'human');

	display.render();
}

function keyDown(event) {
	if(!keyFlag)
		return;
	keyFlag = false;
	console.log(event.keyCode);
	var key = event.keyCode;
	switch(key) {
		case 37:
			player.step(2);
			break;
		case 38:
			player.step(3);
			break;
		case 39:
			player.step(0);
			break;
		case 40:
			player.step(1);
			break;
	}
	drawFrame();
	setTimeout(function() {
		keyFlag = true;
	}, keyDelay);
}

function init () {
	map.init(60, 60);
	map.generate();

	player = new creatures.Human({});
	player.spawn();

	display.init(80, 25);
	
	drawFrame();

	document.addEventListener('keydown', keyDown);
}

init();
},{"./creatures.js":1,"./display.js":3,"./map.js":5}],5:[function(require,module,exports){

var CELLTYPES = {
	FLOOR: 0,
	WALL: 1,
	NONE: 2
};

var EMPTY = {
	type: CELLTYPES.NONE,
	lit: false,
	explored: false
}

var Map = {};


Map.init = function init (sizeX, sizeY) {
	this.width = sizeX;
	this.height = sizeY;

	this.data = [];

	for(var i=0; i<this.width; i++) {
		this.data.push([]);
		for(var j=0; j<this.width; j++) {
			this.data[i].push({
				type: CELLTYPES.FLOOR,
				lit: false,
				explored: false
			});
		}
	}
}

Map.generate = function() {
	for(var i=0; i<this.width; i++) {
		for(var j=0; j<this.height; j++) {
			if(i == 0 || j == 0 || i == this.width - 1 || j == this.height - 1)
				this.data[i][j].type = CELLTYPES.WALL;
			else
				this.data[i][j].type = (i % 3 == j % 3) && (i % 3 == 1) ? CELLTYPES.WALL : CELLTYPES.FLOOR;
		}
	}
}

Map.get = function(x, y) {
	if(x >=0 && y >= 0 && x < this.width && y < this.height)
		return this.data[x][y];
	return EMPTY;
}

Map.canWalk = function(x, y) {
	var cell = this.get(x, y);
	return cell.type == CELLTYPES.FLOOR;
}

Map.types = CELLTYPES;

module.exports = Map;
},{}]},{},[4]);
