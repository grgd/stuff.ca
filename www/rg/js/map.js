
var CELLTYPES = {
	FLOOR: 0,
	WALL: 1,
	NONE: 2
};

var EMPTY = {
	type: CELLTYPES.NONE,
	lit: false,
	explored: false
}

var Map = {};


Map.init = function init (sizeX, sizeY) {
	this.width = sizeX;
	this.height = sizeY;

	this.data = [];

	for(var i=0; i<this.width; i++) {
		this.data.push([]);
		for(var j=0; j<this.width; j++) {
			this.data[i].push({
				type: CELLTYPES.FLOOR,
				lit: false,
				explored: false
			});
		}
	}
}

Map.generate = function() {
	for(var i=0; i<this.width; i++) {
		for(var j=0; j<this.height; j++) {
			if(i == 0 || j == 0 || i == this.width - 1 || j == this.height - 1)
				this.data[i][j].type = CELLTYPES.WALL;
			else
				this.data[i][j].type = (i % 3 == j % 3) && (i % 3 == 1) ? CELLTYPES.WALL : CELLTYPES.FLOOR;
		}
	}
}

Map.get = function(x, y) {
	if(x >=0 && y >= 0 && x < this.width && y < this.height)
		return this.data[x][y];
	return EMPTY;
}

Map.canWalk = function(x, y) {
	var cell = this.get(x, y);
	return cell.type == CELLTYPES.FLOOR;
}

Map.types = CELLTYPES;

module.exports = Map;