
var display = require('./display.js'),
	map = require('./map.js'),
	creatures = require('./creatures.js');

var player;

var keyDelay = 80;
var keyFlag = true;

function drawFrame() {
	var ox = player.x - display.width / 2 | 0;
	var oy = player.y - display.height / 2 | 0;
	for(var i=0; i<display.width; i++) {
		for(var j=0; j<display.height; j++) {
			var cell = map.get(ox + i, oy + j);
			if(cell === undefined)
				console.log(ox + i, oy + j);
			var type;
			switch(cell.type) {
				case map.types.WALL:
					type = 'wall';
					break;
				case map.types.FLOOR:
					type = 'floor';
					break;
				case map.types.NONE:
					type = 'nothing';
					break;
			}
			display.setCell(i, j, type);
		}
	}
	display.setCell(player.x - ox, player.y - oy, 'human');

	display.render();
}

function keyDown(event) {
	if(!keyFlag)
		return;
	keyFlag = false;
	console.log(event.keyCode);
	var key = event.keyCode;
	switch(key) {
		case 37:
			player.step(2);
			break;
		case 38:
			player.step(3);
			break;
		case 39:
			player.step(0);
			break;
		case 40:
			player.step(1);
			break;
	}
	drawFrame();
	setTimeout(function() {
		keyFlag = true;
	}, keyDelay);
}

function init () {
	map.init(60, 60);
	map.generate();

	player = new creatures.Human({});
	player.spawn();

	display.init(80, 25);
	
	drawFrame();

	document.addEventListener('keydown', keyDown);
}

init();