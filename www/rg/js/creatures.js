
var dirs = require('./dirs.js');

var Creatures = {};

var Base = function () {
}

Base.prototype.init = function(options) {
	this.x = options.x || 0;
	this.y = options.y || 0;
	this.char = '@';
};

Base.prototype.step = function(dir) {
	if(map.canWalk(this.x + dirs[dir].x, this.y + dirs[dir].y)) {
		this.x += dirs[dir].x;
		this.y += dirs[dir].y;
	}
};

Base.prototype.spawn = function() {
	while(!map.canWalk(this.x, this.y)) {
		this.x = map.width * Math.random() | 0;
		this.y = map.height * Math.random() | 0;
	}
}

var Human = function(options) {
	this.init(options);
	this.char = '@';
}

Human.prototype = new Base();

Creatures.Human = Human;

module.exports = Creatures;

var map = require('./map.js');