
var objectCells = {
	'wall': {
		char: '#',
		fg: '#FFF',
		bg: '#333'
	},
	'floor': {
		char: '.',
		fg: '#aaa',
		bg: '#000'
	},
	'nothing': {
		char: '0',
		fg: '#000',
		bg: '#000'
	},
	'human': {
		char: '@',
		fg: '#fff'
	}
};

var Display = {};

Display.init = function init (width, height) {
	this.width = width;
	this.height = height;

	this.offsetX = 0;
	this.offsetY = 0;

	this.data = [];
	for(var i=0; i<this.width; i++) {
		this.data.push([]);
		for(var j=0; j<this.height; j++) {
			this.data[i].push({
				char: '.',
				fg: '#FFF',
				bg: '#000'
			});
		}
	}

	this.element = document.getElementById('display');
	for(var j=0; j<this.height; j++) {
		var row = document.createElement('div');
		row.className = 'display-row';
		for(var i=0; i<this.width; i++) {
			var cell = document.createElement('span');
			row.appendChild(cell);
			this.data[i][j].cell = cell;
		}
		this.element.appendChild(row);
	}
}

Display.clear = function(char, fg, bg) {
	for(var i=0; i<this.width; i++) {
		for(var j=0; j<this.height; j++) {
			this.data[i][j].char = char;
			this.data[i][j].fg = fg;
			this.data[i][j].bg = bg;
		}
	}
}

Display.render = function() {
	var c;
	for(var i=0; i<this.width; i++) {
		for(var j=0; j<this.height; j++) {
			c = this.data[i][j];
			c.cell.style.backgroundColor = c.bg;
			c.cell.style.color = c.fg;
			c.cell.innerText = c.char;
		}
	}
}

Display.setCell = function(x, y, object) {
	if(x < 0 || y < 0 || x >= this.width || y >= this.height)
		return;
	var template = objectCells[object];
	var cell = this.data[x][y];
	cell.char = template.char || cell.char;
	cell.fg = template.fg || cell.fg;
	cell.bg = template.bg || cell.bg;
}

module.exports = Display;
