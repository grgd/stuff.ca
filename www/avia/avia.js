(function () {
	var canvas = document.createElement('canvas'),
		container = document.getElementById('container'),
		height = window.innerHeight,
		width = height;
	canvas.width = width;
	canvas.height = height;
	container.style.width = width + 'px';

	var ctx = canvas.getContext('2d');

	container.appendChild(canvas);

	var dt = 0.1,
		maxX = 1.0,
		maxY = 1.0;

	var Block = function(x, y, w, h, vx, vy) {
		this.x = x || 0;
		this.y = y || 0;
		this.h = h || 0.1;
		this.w = w || 0.1;

		this.top = 0;
		this.left = 0;
		this.bottom = 0;
		this.right = 0;
		this.setPos();

		this.vx = vx || 0;
		this.vy = vy || 0;
	}

	Block.prototype.setPos = function(x, y) {
		this.x = x || this.x;
		this.y = y || this.y;

		this.top = this.y;
		this.left = this.x;
		this.bottom = this.y + this.h;
		this.right = this.x + this.w;
	};

	Block.prototype.move = function(dx, dy) {
		this.setPos(this.x + dx, this.y + dy);
	};

	Block.prototype.step = function() {
		this.move(this.vx * dt, this.vy * dt);
	};

	Block.prototype.checkX = function(block) {
		if(this.vx > 0) {
			if(this.right > block.left) {
				this.setPos(block.left - this.w - 1, null);
				this.vx *= -1;
				block.vx *= -1;
			}
		} else {
			if(this.left > block.right) {
				this.setPos(block.right + 1, null);
				this.vx *= -1;
				block.vx *= -1;
			}
		}
	};

	Block.prototype.checkY = function(block) {
		if(this.vy > 0) {
			if(this.bottom > block.top) {
				this.setPos(null, block.top - this.h - 1);
				this.vy *= -1;
				block.vy *= -1;
			}
		} else {
			if(this.top > block.bottom) {
				this.setPos(null, block.bottom + 1);
				this.vy *= -1;
				block.vy *= -1;
			}
		}
	};

	Block.prototype.collide = function(block) {
		if(this.top > block.bottom || this.left > block.right || this.bottom < block.top || this.right < block.left)
			return false;

		/*var dx = (block.x + block.w / 2) - (this.x + this.w / 2) - (this.w / 2 + block.w / 2);
		var dy = (block.y + block.h / 2) - (this.y + this.h / 2) - (this.h / 2 + block.h / 2);
		if(Math.abs(dx) < Math.abs(dy)) {
			this.setPos(this.x + dx + (dx > 0 ? 1 : -1), null);
			this.vx *= -1;
			block.vx *= -1;
		} else {
			this.setPos(null, this.y + dy + (dy > 0 ? 1 : -1));
			this.vy *= -1;
			block.vy *= -1;
		}*/

		var dx = this.x - block.x;
		var dy = this.y - block.y;
		if(dx > 0) {
			dx = block.right - this.left;
		} else {
			dx = block.left - this.right;
		}
		if(dy > 0) {
			dy = block.bottom - this.top;
		} else {
			dy = block.top - this.bottom;
		}
		if(Math.abs(dx) < Math.abs(dy)) {
			this.setPos(this.x + dx + (dx > 0 ? dt : -dt), null);
			this.vx *= -1;
			block.vx *= -1;
		} else {
			this.setPos(null, this.y + dy + (dy > 0 ? dt : -dt));
			this.vy *= -1;
			block.vy *= -1;
		}
	};

	var blocks = [
		new Block(0, -10, width, 15, 0, 0),
		new Block(width - 5, 0, 15, height, 0, 0),
		new Block(0, height - 5, width, 15, 0, 0),
		new Block(-10, 0, 15, height, 0, 0),
		new Block(width / 3, height / 2, width * 0.1, height * 0.1, 2.5, 0),
		new Block(width * 2 / 3, height / 2, width * 0.2, height * 0.05, -1.5, 0)
	];

	function step() {
		var i, j, n = blocks.length;

		for(i=4; i<n; i++)
			blocks[i].step();

		for(i=4; i<n; i++) {
			for(j=0; j<n; j++)
				if(j != i) {
					blocks[i].collide(blocks[j]);
				}
		}
	}

	function draw() {
		for(var i=0; i<1.0 / dt; i++)
			step();

		ctx.fillStyle = "#fff";
		ctx.fillRect(0, 0, width, height);

		ctx.fillStyle = "#000";
		for(i=0; i<blocks.length; i++) {
			var b = blocks[i];
			ctx.fillRect(b.x, b.y, b.w, b.h);
		}
	}

	setInterval(draw, 20);
})();