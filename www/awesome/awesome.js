
(function () {
	var rand = Math.random,
		Color = net.brehaut.Color;
	var canvas = document.createElement('canvas'),
		ctx;

	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
	document.body.appendChild(canvas);

	var userColor = Color({red: 1.0, green: 0.5, blue: 0.5});

	var SIZEX = 40,
		SIZEY = 28,
		tileWidth = window.innerWidth / SIZEX,
		tileHeight = window.innerHeight / SIZEY;

	function initColor() {
		return rand() * 0.5;
	}

	var field = [];

	for(var i=0; i<SIZEX; i++) {
		field[i] = [];
		for(var j=0; j<SIZEY; j++)
			field[i][j] = {
					status: -1, // stable
					color: Color("#505050")//Color({red: initColor(), green: initColor() * 0.9, blue: initColor() * 0.6})
				};
	}

	var isLandscape = true;
	function checkOrientation () {
		isLandscape = window.innerWidth > window.innerHeight;
		tileWidth = window.innerWidth / SIZEX,
		tileHeight = window.innerHeight / SIZEY;
	}

	function redraw () {
		var c;
		if(isLandscape) {
			for(var i=0; i<SIZEX; i++)
				for(var j=0; j<SIZEY; j++) {
					c = field[i][j].color.clone();
					if(field[i][j].status >= 0)
						ctx.fillStyle = c.valueByRatio( (-Math.cos(field[i][j].status * 0.4) + 1) * 0.5  ).toString();
					else
						ctx.fillStyle = c;
					ctx.fillRect(i * tileWidth, j * tileHeight, tileWidth, tileHeight);
				}
		} else {
			ctx.fillStyle = "#343434";
			ctx.fillRect(0, 0, canvas.width, canvas.height);
			ctx.fillStyle = "#AEAEAE";
			ctx.font = "30px Arial";
			ctx.fillText("Rotate", 10, window.innerHeight / 2);
		}
	}

	function confirmColor (x, y, color) {
		field[x][y].status = -1;
		field[x][y].color = Color(color);
	}

	function onClick (event) {
		var x = (event.clientX / tileWidth) | 0,
			y = (event.clientY / tileHeight) | 0;
		if(x < 0 || y < 0 || x >= SIZEX || y >= SIZEY)
			return;
		field[x][y].status = 0;
		socket.emit('paint', {x: x, y: y});
	}

	function resize () {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		ctx = canvas.getContext("2d");
		checkOrientation();
	}

	function frame () {
		for(var i=0; i<SIZEX; i++)
			for(var j=0; j<SIZEY; j++) {
				if(field[i][j].status >= 0) {
					field[i][j].status ++;
				}
			}
		redraw();
	}

	var socket = io(ADDR);

	socket.on('connect', function () {
		socket.send('hi');

		socket.on('greetings', function (data) {
			userColor = Color(data.color);
			for(var i=0; i<SIZEX; i++)
				for(var j=0; j<SIZEY; j++) {
					field[i][j].color = Color(data.field[i][j].color);
				}
			canvas.addEventListener('click', onClick);
			window.addEventListener('resize', resize);
			setInterval(frame, 40);
		});

		socket.on('repaint', function (data) {
				field[data.x][data.y].color = Color(data.color);
				field[data.x][data.y].status = -1;
		});
	});

	resize();
	redraw();
	
})()