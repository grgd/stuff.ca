var io = require('socket.io').listen(800);

var clients = [];

var SIZEX = 40,
    SIZEY = 28,
    DEAD_COLOR = "#505050",
    INIT_COLOR = "#AEAEAE",
    field = [],
    colors = {};

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function generateColor () {
  var a = Math.random() * Math.PI * 0.5,
      r = (Math.sin(a) + 1) * 0.5,
      g = (Math.cos(a) + 1) * 0.5,
      b = Math.random();
  return rgbToHex( (r * 255) | 0, (g * 255) | 0, (b * 255) | 0 );
}

for(var i=0; i<SIZEX; i++) {
  field[i] = [];
  for(var j=0; j<SIZEY; j++) {
    field[i][j] = {
        lastModified: "-",
        color: INIT_COLOR
      };
  }
}

console.log("Server started");

io.sockets.on('connection', function (socket) {
  clients.push(socket);

  console.log("New user arrived");

  colors[socket.id] = generateColor();//rgbToHex( (Math.random() * 128) | 0 + 120, (Math.random() * 128) | 0 + 120, (Math.random() * 128) | 0 + 120);

  socket.emit('greetings', { color: colors[socket.id], field: field});

  console.log("User gained color " + colors[socket.id]);

  socket.on('disconnect', function () {
    console.log("User " + colors[socket.id] + " lost us");
    colors[socket.id] = DEAD_COLOR;
    for(var i=0; i<SIZEX; i++) {
      for(var j=0; j<SIZEY; j++) {
        if(field[i][j].lastModified == socket.id) {
          field[i][j].color = DEAD_COLOR;
          socket.broadcast.emit('repaint', {x: i, y: j, color: DEAD_COLOR});
        }
      }
    }
  });

  socket.on('paint', function (data) {
    console.log("User " + colors[socket.id] + " painted " + data.x + "; " + data.y);
    field[data.x][data.y].lastModified = socket.id;
    field[data.x][data.y].color = colors[socket.id];
    socket.broadcast.emit('repaint', {x: data.x, y: data.y, color: colors[socket.id]});
    socket.emit('repaint', {x: data.x, y: data.y, color: colors[socket.id]});
  });

});