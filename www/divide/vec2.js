var Vec2 = function Vec2 (x, y) {
	this.x = x;
	this.y = y;
}

Vec2.prototype.add = function(v) {
	this.x += v.x;
	this.y += v.y;
};

Vec2.prototype.sub = function(v) {
	this.x -= v.x;
	this.y -= v.y;
};

Vec2.prototype.sqrMag = function() {
	return this.x * this.x + this.y * this.y;
};

Vec2.prototype.set = function(x, y) {
	this.x = x;
	this.y = y;
};

Vec2.prototype.setAdd = function(a, b) {
	this.x = a.x + b.x;
	this.y = a.y + b.y;
};

Vec2.prototype.setSub = function(a, b) {
	this.x = a.x - b.x;
	this.y = a.y - b.y;
};

Vec2.prototype.setSubWrap = function(a, b, w, h) {
	this.x = a.x - b.y;
	if(this.x > w) this.x -= 2 * w;
	if(this.x < -w) this.x += 2 * w;
	this.y = a.y - b.y;
	if(this.y > h) this.y -= 2 * h;
	if(this.y < -h) this.y += 2 * h;
};

Vec2.prototype.scale = function(a) {
	this.x *= a;
	this.y *= a;
};

Vec2.prototype.clamp = function(mag) {
	var l = this.sqrMag();
	if(l > mag * mag) {
		l = mag / Math.sqrt(l);
		this.x *= l;
		this.y *= l;
	}
};

Vec2.prototype.norm = function(mag) {
	var l = (mag + 1) / (Math.sqrt(this.x * this.x + this.y * this.y) + 1);
	this.x *= l;
	this.y *= l;
};


/* adds random perpendicular vector */
Vec2.prototype.tilt = function(mag) {
	var x = this.x;
	this.x += mag * this.y;
	this.y -= mag * x;
};

Vec2.prototype.addScaled = function(v, a) {
	this.x += v.x * a;
	this.y += v.y * a;
};