var M3 = M3 || {};

M3.currentViewer = null;

var FPS = 0;
function collectFPS () {
	document.title = FPS.toString()+' fps';
	FPS = 0;
	setTimeout(collectFPS, 1000);
}

(function() {
  var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                              window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  window.requestAnimationFrame = requestAnimationFrame;
})();

M3.LINE = 0;
M3.FILLEDQUAD = 1;
M3.LINEFILLEDQUAD = 2;
M3.TEXQUAD = 3;
M3.LINETEXQUAD = 4;
M3.SPRITEQUAD = 5;
M3.LINEFILLEDPOLY = 6;
M3.TEXPOLY = 7;
M3.STRETCHTEX = 8;

M3.Viewer = function (canvas) {

	M3.currentViewer = this;

	this.canvas = canvas;
	this.ctx = canvas.getContext("2d");

	this.width = canvas.clientWidth || 640;
	this.height = canvas.clientHeight || 480;

	this.camera = new M3.Camera(2, this.height / this.width, this);

	this.bgColor = "#ca004e";

	this.meshes = [];
	this.faces = [];

	this.mouseX = 0;
	this.mouseY = 0;
	this.drag = false;

	this.mouseAccel = 1.1;
	this.touchAccel = 1.9;
	
	this.mouseX = this.width/2;
	this.mouseY = this.height/2;

	this.pickedMesh = null;
	this.pick = false;
	this.onpick = null;
	this.pickPoint = {cx: 0, cy: 0};
	
	// setup input handlers.
	// compatibility for touch devices is taken into account
	var isTouchDevice = (document.createTouch != undefined);	// detect if it is running on a touch device
	var self = this;
	if(!isTouchDevice) {
		this.canvas.addEventListener('mousedown', function(e){self.onMouseDown(e);}, false);
		this.canvas.addEventListener('mouseup', function(e){self.onMouseUp(e);}, false);
		this.canvas.addEventListener('mousemove', function(e){self.onMouseMove(e);}, false);
	} else {
		//document.title = 'I am touchy!';
		this.canvas.addEventListener('touchstart', function(e){self.touchStart(e);}, false);
		this.canvas.addEventListener('touchend', function(e){self.touchEnd(e);}, false);
		this.canvas.addEventListener('touchmove', function(e){self.touchMove(e);}, false);
	}

	this.ready = true;
	this.needUpdate = true;
	this.timer = 0;
	collectFPS();
}

M3.Viewer.prototype.setClickCallback = function(callback) {
	this.canvas.addEventListener('mousedown', callback, false);
};

M3.Viewer.prototype.removeClickCallback = function(callback) {
	this.canvas.removeEventListener('mousedown', callback, false);
};

M3.Viewer.prototype.pickObject = function(x, y) {
	//for (var i = this.faces.length - 1; i >= 0; i--) {
	for(var i=0; i<this.faces.length; i++) {
		var p = this.faces[i].projected;
		var l = p.length;
		if(l > 2) {
			var sign = null;
			for (var j = 0; j < l; j++) {
				var dx = p[(j+1)%l][0] - p[j][0]; 
				var dy = p[(j+1)%l][1] - p[j][1]; 

				var px = x - p[j][0];
				var py = y - p[j][1];

				var s = dx*py > dy*px ? 1 : -1;
				if(sign == null) sign = s;
				if(s != sign) break;
			};
			if(j == l)
				return this.faces[i].parent;
		}
	};
	return null;
};

M3.Viewer.prototype.getCoords = function(event) {
	var x = y = 0;
    var event = event || window.event;
     
    // Получаем координаты клика по странице, то есть абсолютные координаты клика.
 
    if (document.attachEvent != null) { // Internet Explorer & Opera
        x = window.event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
        y = window.event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
    } else if (!document.attachEvent && document.addEventListener) { // Gecko
        x = event.clientX + window.scrollX;
        y = event.clientY + window.scrollY;
    } 
     
    //Определяем границы объекта, в нашем случае картинки.
    var getOffsetSum = function (elem) {
	    var top=0, left=0
	    while(elem) {
	        top = top + parseFloat(elem.offsetTop)
	        left = left + parseFloat(elem.offsetLeft)
	        elem = elem.offsetParent        
	    }
	    
	    return {top: Math.round(top), left: Math.round(left)}
	}

 	var cc = getOffsetSum(this.canvas);
    y0=cc.top;//this.canvas.offsetTop;
    x0=cc.left;//this.canvas.offsetLeft;
         
    // Пересчитываем координаты и выводим их алертом.
 
    x = x-x0;
    y = y-y0;
     
    this.pickPoint.cx = x;
    this.pickPoint.cy = y;
};

M3.animate = function(viewer) {
	requestAnimationFrame( function() { M3.animate(viewer); } );
	viewer.update();
	FPS++;
}

M3.Viewer.prototype.update = function() {
	if(this.needUpdate) {
		this.render();
		this.needUpdate = false;
	}
};
M3.Viewer.prototype.onMouseDown = function(event) {
	this.mouseX = event.clientX;
	this.mouseY = event.clientY;
	this.drag = true;
	if(this.pick) {
		this.getCoords(event);
		this.pickedMesh = this.pickObject(this.pickPoint.cx, this.pickPoint.cy);
		if(this.onpick)
			this.onpick();
	}
};
M3.Viewer.prototype.onMouseUp = function(event) {
	this.drag = false;
}
M3.Viewer.prototype.onMouseMove = function(event) {
	var rotH = event.clientX - this.mouseX;
	var rotV = event.clientY - this.mouseY;

	if(this.drag) {
		this.camera.orbit(rotH * this.mouseAccel, rotV * this.mouseAccel);
		this.needUpdate = true;
	}

	this.mouseX = event.clientX;
	this.mouseY = event.clientY;
};
M3.Viewer.prototype.touchStart = function(e) {
	e.preventDefault();
	var t = e.changedTouches[0];
	this.mouseX = t.clientX;
	this.mouseY = t.clientY;
	if(this.pick) {
		this.getCoords(event);
		this.pickedMesh = this.pickObject(this.pickPoint.cx, this.pickPoint.cy);
		if(this.onpick)
			this.onpick();
	}
}
M3.Viewer.prototype.touchEnd = function(e) {
	e.preventDefault();
	var t = e.changedTouches[0];
}
M3.Viewer.prototype.touchMove = function(e) {
	e.preventDefault();
	var t = e.changedTouches[0];
	var rotH = t.clientX - this.mouseX;
	var rotV = t.clientY - this.mouseY;
	
	this.camera.orbit(rotH * this.touchAccel, rotV * this.touchAccel);
	this.needUpdate = true;

	this.mouseX = t.clientX;
	this.mouseY = t.clientY;
}

M3.Viewer.prototype.renderQuad = function(face) {
	face.shade = vec3.dot(face.normal, this.camera.look);
	if(face.shade > 0.0000001 || !face.visible)
		return false;
	var c = this.ctx;
	if(face.material.kind != M3.LINE)
		this.renderTexQuaq(face);
	else {
		c.beginPath();
		c.strokeStyle = face.material.lineColor;
		c.lineWidth = face.material.lineWidth;
		c.lineJoin="round";
		c.moveTo(face.projected[0][0], face.projected[0][1]);
		var l = face.transformed.length;
		for(var j=1; j<l; j++) {
			c.lineTo(face.projected[j][0], face.projected[j][1])
		}
		c.stroke();
	}
}
M3.Viewer.prototype.renderTexQuaq = function(face) {
	var x0 = face.projected[0][0];
	var x1 = face.projected[1][0];
	var x2 = face.projected[2][0];
	var y0 = face.projected[0][1];
	var y1 = face.projected[1][1];
	var y2 = face.projected[2][1];
	var l = face.projected.length;
	if(l == 6) {
		x0 = face.projected[1][0];
		x1 = face.projected[0][0];
		x2 = face.projected[l-1][0];
		y0 = face.projected[1][1];
		y1 = face.projected[0][1];
		y2 = face.projected[l-1][1];

		x2 = x1 + face.projected[2][0] - x0;
		y2 = y1 + face.projected[2][1] - y0;
	}
	x1 -= x0;
	y1 -= y0;
	x2 -= x0;
	y2 -= y0;

	var m = face.material;

	var u0 = 0;
	var u1 = m.ratio * face.width;
	var u2 = m.ratio * face.width;
	var v0 = 0;
	var v1 = 0;
	var v2 = m.ratio * face.height;
	if(m.kind == M3.STRETCHTEX) {
		u2 = u1 = m.texWidth;
		v2 = m.texHeight;
	}

	var det = 1 / (u1*v2 - u2*v1),

	// linear transformation
	a = (v2*x1 - v1*x2) * det,
	b = (v2*y1 - v1*y2) * det,
	c = (u1*x2 - u2*x1) * det,
	d = (u1*y2 - u2*y1) * det,

	// translation
	e = x0 - a*u0 - c*v0,
	f = y0 - b*u0 - d*v0;

	/*if(this.pick && this.pickedMesh == null) {
		var cx = this.pickPoint.cx;
		var cy = this.pickPoint.cy;
		var px = a * cx + c * cy + e;
		var py = b * cx + d * cy + f;
		if(px > 0 && px < u1 && py > 0 && py < v2) {
			this.pickedMesh = face.parent;
			this.pickPoint.cx = -1000;
			if(this.onpick)
				this.onpick();
		}
	}*/

	this.ctx.save();
	this.ctx.transform(a, b, c, d, e, f);
	var c = this.ctx;
	switch(m.kind) {
		case M3.FILLEDQUAD: 
			c.fillStyle = m.color;
			c.fillRect(0, 0, face.width * m.ratio, face.height * m.ratio);
		break;

		case M3.LINEFILLEDQUAD:
			c.beginPath();
			c.fillStyle = m.color;
			c.strokeStyle = m.lineColor;
			c.lineWidth = m.lineWidth;
			if(face.projected.length < 5) {
				c.rect(0, 0, face.width * m.ratio, face.height * m.ratio);
			} else {
				var p = face.vertices;
				var l = p.length;
				c.moveTo(p[0][0] * face.width * m.ratio, p[0][1] * face.height * m.ratio);
				for (var i = 0; i<l; i++) {
					c.lineTo(p[i][0] * face.width * m.ratio, p[i][1] * face.height * m.ratio);
				};
			}
			//c.closePath();
			c.fill(); c.stroke();
		break;

		case M3.TEXQUAD:
			if(m.texture) {
				c.fillStyle = m.pattern;
				if(face.projected.length < 5) {
					c.fillRect(0, 0, face.width * m.ratio, face.height * m.ratio);
				} else {
					var p = face.vertices;
					var l = p.length;
					c.moveTo(p[0][0] * face.width * m.ratio, p[0][1] * face.height * m.ratio);
					for (var i = 0; i<l; i++) {
						c.lineTo(p[i][0] * face.width * m.ratio, p[i][1] * face.height * m.ratio);
					};
					c.fill();
				}
			}
		break;

		case M3.LINETEXQUAD:
			if(m.texture) {
				c.beginPath();
				c.fillStyle = m.pattern;
				c.strokeStyle = m.lineColor;
				c.lineWidth = m.lineWidth;
				if(face.projected.length < 5) {
					c.rect(0, 0, face.width * m.ratio, face.height * m.ratio);
				} else {
					var p = face.vertices;
					var l = p.length;
					c.moveTo(p[0][0] * face.width * m.ratio, p[0][1] * face.height * m.ratio);
					for (var i = 0; i<l; i++) {
						c.lineTo(p[i][0] * face.width * m.ratio, p[i][1] * face.height * m.ratio);
					};
				}
				c.fill(); c.stroke();
			}
		break;

		case M3.SPRITEQUAD:
			if(m.texture) {
				c.drawImage( m.texture, 0, 0 );
			}
		break;

		case M3.STRETCHTEX:
			if(m.texture) {
				c.drawImage( m.texture, 0, 0 );
			}
		break;
	}
	if(m.shaded == true) {
		face.shade = (1 + face.shade) * (1 + face.shade);
		c.globalAlpha = face.shade;
		c.fillStyle = "black";
		c.fillRect(0, 0, face.width * m.ratio, face.height * m.ratio);
		c.globalAlpha = 1;
	}
	this.ctx.restore();
};

M3.Viewer.prototype.render = function () {
	this.transformScene();

	this.faces.sort(function(a, b) {
		return a.z - b.z;
	});
	this.scale = 1;
	this.ctx.lineCap = "round";
	this.ctx.lineJoin = "round";
	this.ctx.fillStyle = this.bgColor;
	this.ctx.fillRect(0, 0, this.width, this.height);
	this.pickedMesh = null;
	for (var i = this.faces.length - 1; i >= 0; i--) {
		this.renderQuad(this.faces[i]);
	};
}

M3.Viewer.prototype.transformScene = function () {
	this.faces = [];
	var proj = this.camera.matrix;
	var scale = this.camera.scale;
	var offset = this.camera.offset;
	var face;
	for (var i = this.meshes.length - 1; i >= 0; i--) {
		var m = this.meshes[i];
		m.project(proj, scale, offset, this.faces);
	};
}

M3.Viewer.prototype.addMesh = function(mesh) {
	this.meshes.push(mesh);
	this.needUpdate = true;
};

M3.Viewer.prototype.clear = function() {
	this.meshes = [];
};

M3.Viewer.prototype.removeMeshByName = function(name) {
	var i = this.meshes.length;
	while(i--) {
		if(this.meshes[i].name == name)
			this.meshes.splice(i, 1);
	}
};

M3.Viewer.prototype.removeMesh = function(mesh) {
	for (var i = this.meshes.length - 1; i >= 0; i--) {
		if(this.meshes[i] === mesh) {
			this.meshes.splice(i, 1);
			break;
		}
	};
};

M3.Viewer.prototype.setScale = function(new_scale) {
	this.camera.scale = -new_scale * 100;
};

M3.Camera = function (width, aspect, scene) {
	
	this.pos = vec3.fromValues(5, 5, 5);
	this.target = vec3.fromValues(0,0,0);
	this.up = vec3.fromValues(0,0,1);
	
	this.offset = vec4.fromValues(320, 260, 0, 1);
	this.scale = -140;

	this.viewer = scene;	

	this.rotH = Math.PI / 4;
	this.rotV = Math.PI / 4;
	this.dist = 5;

	this.matrix = mat4.create();
	this.hW = width / 2;
	this.hH = width * aspect / 2;

	this.look = vec3.create();
	this.lookAt(0, 0, 0);
}

M3.Camera.prototype.setPos = function(x, y, z) {
	this.target = vec3.fromValues(x, y, z);
	this.lookAt();
};

M3.Camera.prototype.lookAt = function (x, y, z) {
	//mat4.ortho(this.matrix, -this.hW, this.hW, -this.hH, this.hH, 0.01, 40);
	//mat4.perspective(this.matrix, 60, 0.5, 0.01, 40);
	if(x != null)
		vec4.set(this.target, x, y, z);
	//mat4.lookAt(this.proj, this.pos, this.target, this.up);
	mat4.lookAt(this.matrix, this.pos, this.target, this.up);

	/*this.look[0] = this.target[0] - this.pos[0];
	this.look[1] = this.target[1] - this.pos[1];
	this.look[2] = this.target[2] - this.pos[2];*/
	this.look[0] = - this.pos[0];
	this.look[1] = - this.pos[1];
	this.look[2] = - this.pos[2];
	vec3.normalize(this.look, this.look);
}

M3.Camera.prototype.orbit = function (rotH, rotV) {
	this.rotH += rotH * 0.001;
	this.rotV -= rotV * 0.001;

	if(this.rotH > Math.PI/2-0.1) this.rotH = Math.PI/2-0.1;
	if(this.rotH < 0.1) this.rotH = 0.1;
	if(this.rotV > Math.PI/2-0.1) this.rotV = Math.PI/2-0.1;
	if(this.rotV < 0.1) this.rotV = 0.1;

	var sh = Math.sin(this.rotH);
	var ch = Math.cos(this.rotH);
	var sv = Math.sin(this.rotV);
	var cv = Math.cos(this.rotV);

	var a = this.target;

	vec3.set(this.pos, a[0] + ch * sv * this.dist, a[1] + sh * sv * this.dist, a[2] + cv * this.dist);
	this.lookAt();
	//this.viewer.render();
}

M3.Mesh = function (name) {
	this.vertices = []; // array
	this.faces = [];
	this.transformed = []; // array
	this.projected = [];

	this.material = new M3.Material("#AD1DA5", "#FFF", 1.5, null);

	this.name = name;

	this.pos = vec3.fromValues(0,0,0);
	this.scale = vec3.fromValues(1,1,1);
	this.rotation = vec3.fromValues(0,0,0);
	this.transformation = mat4.create();

	this.updateMatrix();
}

M3.Mesh.prototype.updateMatrix = function () {
	mat4.identity(this.transformation);
	
	/*mat4.rotateX(this.transformation, this.transformation, this.rotation[0]);
	mat4.rotateY(this.transformation, this.transformation, this.rotation[1]);
	mat4.rotateZ(this.transformation, this.transformation, this.rotation[2]);*/

	mat4.translate(this.transformation, this.transformation, this.pos);
	mat4.scale(this.transformation, this.transformation, this.scale);

	this.transform(this.transformation);
}

M3.Mesh.prototype.setPos = function(ax, ay, az) {
	this.pos[0] = ax;
	this.pos[1] = ay;
	this.pos[2] = az;

	this.updateMatrix();
}

M3.Mesh.prototype.hide = function() {
	for (var i = this.faces.length - 1; i >= 0; i--) {
		this.faces[i].visible = false;
	};
};

M3.Mesh.prototype.show = function() {
	for (var i = this.faces.length - 1; i >= 0; i--) {
		this.faces[i].visible = true;
	};
};

M3.Mesh.prototype.setScale = function(sx, sy, sz) {
	this.scale[0] = sx;
	this.scale[1] = sy;
	this.scale[2] = sz;

	this.updateMatrix();
};

M3.Mesh.prototype.setRotation = function(rx, ry, rz) {
	this.rotation[0] = rx;
	this.rotation[1] = ry;
	this.rotation[2] = rz;

	this.updateMatrix();
};

M3.Mesh.prototype.addVertex = function(x, y, z) {
	var v = vec4.fromValues(x, y, z, 1);
	this.vertices.push(v);
	this.transformed.push(vec4.clone(v));
	this.projected.push(vec4.create());
};

M3.Mesh.prototype.addVertices = function(v) {
	for(var i=0; i < v.length; i++) {
		this.vertices.push(v[i]);
		this.transformed.push(vec4.clone(v[i]));
		this.projected.push(vec4.create());
	}
};

M3.Mesh.prototype.addFace = function(index) {
	var f = new M3.Face();
	for (var i = 0; i < index.length; i++) {
		f.vertices.push(this.vertices[ index[i] ]);
		f.transformed.push(this.transformed[ index[i] ]);
		f.projected.push(this.projected[ index[i] ]);
	};
	f.calcNormal();
	f.material = this.material;
	f.parent = this;
	this.faces.push(f);
};

M3.Mesh.prototype.transform = function(mat) {
	for (var i = this.vertices.length - 1; i >= 0; i--) {
		vec4.transformMat4(this.transformed[i], this.vertices[i], mat);
	};
	for (var i = this.faces.length - 1; i >= 0; i--) {
		var f = this.faces[i];
		if(f.vertices.length > 2) {
			var v = vec4.create();
			vec4.sub(v, f.transformed[0], f.transformed[1]);
			f.width = vec4.length(v);
			vec4.sub(v, f.transformed[1], f.transformed[2]);
			f.height = vec4.length(v);
		}
		vec4.transformMat4(f.normal, f.normal, mat);
		vec3.normalize(f.normal, f.normal);
	};
};

M3.Mesh.prototype.project = function(mat, scale, offset, buffer) {
	for (var i = this.vertices.length - 1; i >= 0; i--) {
		// TODO: unroll
		vec4.transformMat4(this.projected[i], this.transformed[i], mat);
		vec4.scaleAndAdd(this.projected[i], offset, this.projected[i], scale);

	};
	for (var i = this.faces.length - 1; i >= 0; i--) {
		var f = this.faces[i];
		var l = f.projected.length;
		f.z = 0;//f.projected[0][2];
		for (var j = 1; j < l; j++) { // z-index calculation
			f.z = f.z + f.projected[j][2];//f.z > f.projected[j][2] ? f.z : f.projected[j][2];
		};
		f.z /= l;
		buffer.push(f);
	};
};

M3.Mesh.prototype.makeCube = function() {
	this.addVertex(0, 0, 0); this.addVertex(1, 0, 0); this.addVertex(1, 1, 0); this.addVertex(0, 1, 0);
	this.addVertex(0, 0, 1); this.addVertex(1, 0, 1); this.addVertex(1, 1, 1); this.addVertex(0, 1, 1);
	
	/*this.addFace([0, 3, 2, 1]); this.addFace([0, 1, 5, 4]); this.addFace([0, 4, 7, 3]);
	this.addFace([6, 7, 4, 5]); this.addFace([6, 5, 1, 2]); this.addFace([6, 2, 3, 7]);*/
	this.addFace([0, 3, 2, 1]); this.addFace([5, 4, 0, 1]); this.addFace([4, 7, 3, 0]);
	this.addFace([6, 7, 4, 5]); this.addFace([6, 5, 1, 2]); this.addFace([7, 6, 2, 3]);
};

M3.Mesh.prototype.makeCorner = function(chip) {
	var v = 1.0 - chip;
	this.addVertex(0, 1, 0); this.addVertex(0, 0, 0); this.addVertex(1, 0, 0); this.addVertex(1, v, 0); this.addVertex(v, v, 0); this.addVertex(v, 1, 0); 
	this.addVertex(0, 1, 1); this.addVertex(0, 0, 1); this.addVertex(1, 0, 1); this.addVertex(1, v, 1); this.addVertex(v, v, 1); this.addVertex(v, 1, 1);

	this.addFace([1, 0, 5, 4, 3, 2]); this.addFace([8, 7, 1, 2]); this.addFace([7, 6, 0, 1]); this.addFace([9, 8, 2, 3]);  this.addFace([10, 9, 3, 4]);
	this.addFace([11, 10, 4, 5]);  this.addFace([6, 11, 5, 0]);  this.addFace([6, 7, 8, 9, 10, 11]);

	this.top = this.faces[7];
};

M3.Mesh.prototype.makeBillboard = function(wid, hei) {
	var a = wid / 2.8284271;
	this.addVertex(-a, a, 0); this.addVertex(a, -a, 0); this.addVertex(a, -a, hei); this.addVertex(-a, a, hei);
	this.addFace([3, 2, 1, 0]);
};

M3.Mesh.prototype.makeBlock = function(dir) {
	this.addVertex(0, 0, 0); this.addVertex(1, 0, 0); this.addVertex(1, 1, 0); this.addVertex(0, 1, 0);
	this.addVertex(0, 0, 1); this.addVertex(1, 0, 1); this.addVertex(1, 1, 1); this.addVertex(0, 1, 1);
	if(dir) { //X
		this.addVertex(1.02, 1, 1); this.addVertex(1.02, 0, 1); this.addVertex(1.02, 0, 0); this.addVertex(1.02, 1, 0);
	} else { //Y
		this.addVertex(0, 1.02, 1); this.addVertex(1, 1.02, 1); this.addVertex(1, 1.02, 0); this.addVertex(1.02, 0, 0);
	}

	this.addFace([0, 3, 2, 1]); this.addFace([5, 4, 0, 1]); this.addFace([4, 7, 3, 0]);
	this.addFace([6, 7, 4, 5]); this.addFace([6, 5, 1, 2]); this.addFace([7, 6, 2, 3]);
	this.addFace([8, 9, 10, 11]);

	this.top = this.faces[3];
	this.front = this.faces[6];
};

M3.Mesh.prototype.setColor = function(col) {
	for (var i = this.faces.length - 1; i >= 0; i--) {
		this.faces[i].material.color = col;
	};
}
M3.Mesh.prototype.setMaterial = function(col, lcol, lwid, kind, shade, tex) {
	this.material = new M3.Material(col, lcol, lwid, kind, shade, tex);
	for (var i = this.faces.length - 1; i >= 0; i--) {
		this.faces[i].material = this.material;
	};
}

M3.Mesh.prototype.setBakedMaterial = function(mat) {
	this.material = mat;
	for (var i = this.faces.length - 1; i >= 0; i--) {
		//if(this.front != undefined && this.faces[i] !== this.front)
		this.faces[i].material = this.material;
	};
};

M3.Face = function () {
	this.vertices = [];
	this.transformed = [];
	this.projected = [];

	this.normal = vec4.create();
	this.normal[3] = 0; // hack
	this.center = vec4.create();
	this.z = 0; // depth
	this.width = 0; // for
	this.height = 0;// texturing
	this.shade = 0.0;

	this.material = new M3.Material('#134244', '#FFFFFF', 2, null);
	this.parent = null;
	this.visible = true;
}

M3.Face.prototype.calcNormal = function() {
	if(this.vertices.length > 2) {
		var v1 = vec4.create(), v2 = vec4.create();
		vec4.sub(v1, this.vertices[1], this.vertices[0]);
		vec4.sub(v2, this.vertices[2], this.vertices[1]);
		vec3.cross(this.normal, v1, v2);
		vec3.normalize(this.normal, this.normal);
	} else {
		vec3.set(this.normal, 0, 0, 0);
	}
};

M3.Face.prototype.set = function(v) {
	this.vertices = [];
	for (var i = 0; i < v.length; i++) {
		this.vertices.push(v[i]);
	};
	this.calcNormal();
};

M3.Material = function (col, lcol, lwid, kind, shade, tex) {
	this.texture = tex;
	this.gradient = null;
	if(tex != null) {
		this.pattern = M3.currentViewer.ctx.createPattern(this.texture, "repeat");
	} else
		this.pattern = null;
	this.color = col;
	this.lineColor = lcol,
	this.lineWidth = lwid;

	this.kind = kind != undefined ? kind : M3.LINEFILLEDQUAD;
	this.shaded = shade != undefined ? shade : true;
	this.ratio = 100;
}

M3.Material.prototype.loadTexture = function(name) {
	this.texture = new Image();
	var self = this;
	this.texture.onload = function () {
		this.onload = null;
		self.pattern = M3.currentViewer.ctx.createPattern(self.texture, "repeat");
		M3.currentViewer.needUpdate = true;
		self.texWidth = this.width;
		self.texHeight = this.height;
	};
	this.texture.src = name;
};

M3.Arrow = function (x0, y0, z0, x1, y1, z1, nx, ny, nz) {
	this.id = Math.floor(Math.random()*10000).toString();
	this.head = new M3.Mesh('h'+this.id);
	this.tail = new M3.Mesh('t'+this.id);
	this.body = new M3.Mesh('b'+this.id);

	this.size = 0.2;

	if(nx == undefined) {
		nx = 0;
		ny = 0;
		nz = 1;
	}

	this.material = new M3.Material("#FFF", "#dc598c", 3, M3.LINE, false, null);
	this.material.kind = M3.LINE;
	this.head.material = this.material;
	this.tail.material = this.material;
	this.body.material = this.material;

	this.normal = vec3.fromValues(nx, ny, nz);
	vec3.normalize(this.normal, this.normal);

	this.headPoint = vec4.fromValues(x1, y1, z1, 1);
	this.tailPoint = vec4.fromValues(x0, y0, z0, 1);
	this.hf1 = vec4.fromValues(0, 0, 0, 1);
	this.hf2 = vec4.fromValues(0, 0, 0, 1);
	this.tf1 = vec4.fromValues(0, 0, 0, 1);
	this.tf2 = vec4.fromValues(0, 0, 0, 1);
	
	this.updateFeathers();
	
	this.head.addVertices([this.headPoint, this.hf1, this.hf2]);
	this.head.addFace([0,1]); this.head.addFace([0,2]);
	
	this.tail.addVertices([this.tailPoint, this.tf1, this.tf2]);
	this.tail.addFace([0,1]); this.tail.addFace([0,2]);
	
	this.body.addVertices([this.headPoint, this.tailPoint]);
	this.body.addFace([0,1]);

	this.gray();
}

M3.Arrow.prototype.set = function(x0, y0, z0, x1, y1, z1) {
	//this.headPoint = vec4.fromValues(x1, y1, z1, 1);
	//this.tailPoint = vec4.fromValues(x0, y0, z0, 1);
	vec4.set(this.headPoint, x1, y1, z1, 1);
	vec4.set(this.tailPoint, x0, y0, z0, 1);
	this.updateFeathers();

	this.head.updateMatrix();
	this.tail.updateMatrix();
	this.body.updateMatrix();
};

M3.Arrow.prototype.addToScene = function(scene) {
	scene.addMesh(this.head);
	scene.addMesh(this.tail);
	scene.addMesh(this.body);
}

M3.Arrow.prototype.remove = function(scene) {
	scene.removeMesh(this.head);
	scene.removeMesh(this.tail);
	scene.removeMesh(this.body);
};

M3.Arrow.prototype.highlight = function() {
	this.material.lineColor = "#feff95";
	this.head.show();
	this.tail.show();
	this.body.show();
};

M3.Arrow.prototype.gray = function() {
	this.material.lineColor = "#ca004e";
	this.head.hide();
	this.tail.hide();
	this.body.hide();
};

M3.Arrow.prototype.updateFeathers = function() {
	var dir = vec4.create();
	vec4.sub(dir, this.tailPoint, this.headPoint);
	vec4.normalize(dir, dir);
	var m = mat4.create();
	mat4.rotate(m, m, Math.PI/2, this.normal);
	var left = vec4.create();
	vec4.transformMat4(left, dir, m);

	this.hf1[0] = this.headPoint[0] + (dir[0] + left[0] * 0.5) * this.size;
	this.hf1[1] = this.headPoint[1] + (dir[1] + left[1] * 0.5) * this.size;
	this.hf1[2] = this.headPoint[2] + (dir[2] + left[2] * 0.5) * this.size;

	this.hf2[0] = this.headPoint[0] + (dir[0] - left[0] * 0.5) * this.size;
	this.hf2[1] = this.headPoint[1] + (dir[1] - left[1] * 0.5) * this.size;
	this.hf2[2] = this.headPoint[2] + (dir[2] - left[2] * 0.5) * this.size;

	this.tf1[0] = this.tailPoint[0] - (dir[0] + left[0] * 0.5) * this.size;
	this.tf1[1] = this.tailPoint[1] - (dir[1] + left[1] * 0.5) * this.size;
	this.tf1[2] = this.tailPoint[2] - (dir[2] + left[2] * 0.5) * this.size;

	this.tf2[0] = this.tailPoint[0] - (dir[0] - left[0] * 0.5) * this.size;
	this.tf2[1] = this.tailPoint[1] - (dir[1] - left[1] * 0.5) * this.size;
	this.tf2[2] = this.tailPoint[2] - (dir[2] - left[2] * 0.5) * this.size;
}

M3.Table = function(lwid, rwid, hei, dep) {
	this.lwidth = lwid;
	this.rwidth = rwid;
	this.height = hei;
	this.depth = dep;
	
	this.body = new M3.Mesh('table');
	
	this.v = [vec4.fromValues(0,0,0,1), vec4.fromValues(lwid,0,0,1),
	vec4.fromValues(lwid,0,0,1), vec4.fromValues(0,0,0,1)];
}

M3.Minecraft = function () {
	this.blocks = [];

}