function noise(x, y) {
  /*var n = x + y * 57;
  n = (n << 13) ^ n;
  n = (n * (n * n * 15731 + 789221) + 1376312589);
  n = n & 2147483647;
  n = n / 1073741824.0;
  n = 1.0 - n;
  return n;*/
//  return ( 1.0 - ( (n * (n * n * 15731 + 789221) + 1376312589) & 2147483647) / 1073741824.0);
	var n = x + y * 57;
	n = (n * 15731 + 43) % 113;
	return n / 113.0;
}

function cosInterp(a, b, x) {
	var f = (1 - Math.cos(x * 3.1415926)) * 0.5;
	return a * (1.0 - f) + b * f;
}

function octave(x, y) {
	var cx = Math.floor(x);
	var cy = Math.floor(y);
	var fx = x - cx;
	var fy = y - cy;
	
	var v1 = noise(cx, cy);
	var v2 = noise(cx+1, cy);
	var v3 = noise(cx, cy+1);
	var v4 = noise(cx+1, cy+1);
	
	v1 = cosInterp(v1, v2, fx);
	v3 = cosInterp(v3, v4, fx);
	
	return cosInterp(v1, v3, fy);
}

function perlin(x, y, f) {
	x += f;
	y += f;
	
	var freq = 0.2;
	var amp = 0.8;
	var pers = 0.5;
	
	var val = 0;
	
	for(var i=0; i<4; i++) {
		val += octave(x * freq, y * freq) * amp;
		amp *= pers;
		freq *= pers;
	}
	
	return val;
}
