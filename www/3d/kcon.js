var KCON = KCON || {};

KCON.active = null;

KCON.MAX = {
	lwidth: 5,
	rwidth: 5,
	width: 10,
	height: 1.8,
	depth: 1.5,
	hdepth: 1,
	hheight: 1.2,
	hoffset: 1
};

KCON.MIN = {
	lwidth: 0.5,
	rwidth: 0.5,
	width: 0.6,
	height: 0.5,
	depth: 0.3,
	hdepth: 0.3,
	hheight: 0.4,
	hoffset: 0.4
};

KCON.average = function (param) {
	return Math.round((KCON.MAX[param] + KCON.MIN[param]) / 2);
}

KCON.Constructor = function (canvas_id, div_id) {
	var canvas = document.getElementById(canvas_id);
	this.viewer = new M3.Viewer(canvas);
	this.container = document.getElementById(div_id);
	KCON.active = this;
	this.balloons = [];
}

KCON.Constructor.prototype.addBalloon = function(balloon) {
	balloon.container = this.container;
	this.balloons.push(balloon);
	this.container.appendChild(balloon.div);
};

KCON.Constructor.prototype.clearBalloons = function() {
	for (var i = this.balloons.length - 1; i >= 0; i--) {
		this.container.removeChild(this.balloons[i].div);
	};
	this.balloons = [];
};

KCON.Balloon = function () {
	var div = document.createElement('div');
	div.className = 'balloon';
	var mark = document.createElement('div');
	mark.className = 'balloon-mark';
	div.appendChild(mark);
	var p = document.createElement('p');
	mark.appendChild(p);

	this.container = null;
	this.div = div;
	this.mark = mark;
	this.markTitle = p;
	
	this.content = [];
}

KCON.Balloon.prototype.destroy = function() {
	
};

KCON.Balloon.prototype.clear = function() {
	for (var i = this.content.length - 1; i >= 0; i--) {
		this.div.removeChild(this.content[i].block);
	};
	this.content = [];
};

KCON.Balloon.prototype.addText = function (text) {
	var t = new KCON.Text(text);
	this.addElement(t);
};

KCON.Balloon.prototype.addInput = function(label, param, callbackIn, callbackOut, init) {
	var i = new KCON.Input(label, param, ' см.', callbackIn, callbackOut, Math.floor(init * 100).toString());
	this.addElement(i);
};

KCON.Balloon.prototype.addButton = function(title, action) {
	var b = new KCON.Button(title, action);
	this.addElement(b);
};

KCON.Balloon.prototype.addCheckbox = function(title, state, callback) {
	var c = new KCON.Checkbox(title, state, callback);
	this.addElement(c);
};

KCON.Balloon.prototype.addElement = function(e) {
	this.div.appendChild(e.block);
	this.content.push(e);
}

KCON.Balloon.prototype.setXY = function(x, y) {
	this.div.style.left = x.toString()+'px';
	this.div.style.top = y.toString()+'px';
};

KCON.Balloon.prototype.setMark = function(char) {
	this.markTitle.innerHTML = char;
};

KCON.Text = function (title) {
	this.block = document.createElement('p');
	this.block.innerHTML = title;
};

KCON.Input = function (title, param, postfix, callbackIn, callbackOut, init) {
	this.field = document.createElement('input');
	this.field.type = 'number';
	this.field.value = init;
	this.field.maxLenght = 3;
	this.field.style.width = '60px';
	this.field.onfocus = function () {
		callbackIn(param);
	}
	this.field.onblur = function () {
		var success = true;
		var s = this.value;
		var v = parseInt(s, 10);
		if(!isNaN(v)) {
			s = callbackOut(param, v / 100.0);
		} else
			s = callbackOut(param, (KCON.MIN[param]+KCON.MAX[param])/2 );

		this.value = s.toString();
	}
	this.block = document.createElement('p');
	this.block.appendChild(document.createTextNode(title));
	this.block.appendChild(this.field);
	this.block.appendChild(document.createTextNode(postfix));
}

KCON.Button = function (title, action) {
	this.button = document.createElement('button');
	this.button.onclick = action;
	this.button.innerHTML = title;
	this.block = document.createElement('p');
	this.block.appendChild(this.button);
}

KCON.Checkbox = function (title, state, callback) {
	this.box = document.createElement('input');
	this.box.type = 'checkbox';
	this.box.checked = state || false;
	this.box.onclick = callback;
	this.block = document.createElement('p');
	this.block.appendChild(document.createTextNode(title));
	this.block.appendChild(this.box);
}
