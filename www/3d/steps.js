
KCON.Constructor.prototype.step1 = function() {
	var balloon1 = new KCON.Balloon();
	balloon1.setMark('?');
	
	balloon1.addText('Выберите тип кухни:');
	
	balloon1.addButton('Угловая', function() {
		KCON.active.step2a();
	});
	
	balloon1.addButton('Прямая', function() {
		KCON.active.step2b();
	});
	
	this.addBalloon(balloon1);

	M3.animate(this.viewer);
};

/***********************************************/
/***********************************************/
/***********************************************/
KCON.Constructor.prototype.step2a = function() {
	this.kitchen =  this.kitchen || {
		kind: 'corner',
		lwidth: (KCON.MAX.lwidth + KCON.MIN.lwidth) / 2,
		rwidth: (KCON.MAX.rwidth + KCON.MIN.rwidth) / 2,
		height: (KCON.MAX.height + KCON.MIN.height) / 2,
		depth: (KCON.MAX.depth + KCON.MIN.depth) / 2
	};

	var self = this;
	var k = this.kitchen;
	this.models = {};
	this.viewer.clear();
	var m = this.models;
	var mat = new M3.Material("#ca004e", "#FFF", 3, M3.LINEFILLEDQUAD, true, null);

	var c = new M3.Mesh();
	c.makeCube();
	c.setBakedMaterial(mat);
	this.viewer.addMesh(c);
	m.corner = c;

	c = new M3.Mesh();
	c.makeCube();
	c.setBakedMaterial(mat);
	this.viewer.addMesh(c);
	m.left = c;

	c = new M3.Mesh();
	c.makeCube();
	c.setBakedMaterial(mat);
	this.viewer.addMesh(c);
	m.right = c;

	c = new M3.Arrow(0.1, -0.2, k.height + 0.1, k.lwidth + k.depth, -0.2, k.height + 0.1);
	c.addToScene(this.viewer);
	m.larrow = c;

	c = new M3.Arrow(-0.2, 0.1, k.height + 0.1, -0.2, k.rwidth + k.depth, k.height + 0.1);
	c.addToScene(this.viewer);
	m.rarrow = c;

	c = new M3.Arrow(k.lwidth + k.depth + 0.2, 0, 0, k.lwidth + k.depth + 0.2, 0, k.height, 0, 1, 0);
	c.addToScene(this.viewer);
	m.harrow = c;

	c = new M3.Arrow(0, k.rwidth + k.depth + 0.2, 0, k.depth, k.rwidth + k.depth + 0.2, 0);
	c.addToScene(this.viewer);
	m.darrow = c;

	var updateScene = function () {
		m.corner.setScale(k.depth, k.depth, k.height);
		m.left.setScale(k.lwidth, k.depth, k.height);
		m.left.setPos(k.depth, 0, 0);
		m.right.setScale(k.depth, k.rwidth, k.height);
		m.right.setPos(0, k.depth, 0);
		m.larrow.set(0.1, -0.2, k.height + 0.1, k.lwidth + k.depth, -0.2, k.height + 0.1);
		m.rarrow.set(-0.2, 0.1, k.height + 0.1, -0.2, k.rwidth + k.depth, k.height + 0.1);
		m.harrow.set(k.lwidth + k.depth + 0.2, 0, 0, k.lwidth + k.depth + 0.2, 0, k.height);
		m.darrow.set(0, k.rwidth + k.depth + 0.2, 0, k.depth, k.rwidth + k.depth + 0.2, 0);
		self.viewer.setScale(2.5 / Math.max(k.lwidth, k.rwidth));
		self.viewer.needUpdate = true;
	};

	var selectArrow = function (param) {
		m[param.charAt(0)+'arrow'].highlight();
		self.viewer.needUpdate = true;
	};

	var changeParam = function (param, value) {
		m[param.charAt(0)+'arrow'].gray();
		var v = Math.min(KCON.MAX[param], Math.max(value, KCON.MIN[param]));
		k[param] = v;
		updateScene();
		return Math.floor(v * 100);
	}

	updateScene();

	this.clearBalloons();

	var b = new KCON.Balloon();
	b.setMark('i');
	b.setXY(600, 100);
	b.addInput('Ширина по правой стороне: ', "lwidth", selectArrow, changeParam, k.lwidth);
	b.addInput('Ширина по левой стороне: ', "rwidth", selectArrow, changeParam, k.rwidth);
	b.addInput('Высота: ', "height", selectArrow, changeParam, k.height);
	b.addInput('Глубина: ', "depth", selectArrow, changeParam, k.depth);
	b.addButton('Готово', function() {
		KCON.active.step3a();
	});
	this.addBalloon(b);
};

/***********************************************/
/***********************************************/
/***********************************************/
KCON.Constructor.prototype.step2b = function() {
	this.kitchen = this.kitchen || {
		kind: 'straight',
		width: (KCON.MAX.lwidth * 1.8 + KCON.MIN.lwidth * 0.2) / 2,
		height: (KCON.MAX.height + KCON.MIN.height) / 2,
		depth: (KCON.MAX.depth + KCON.MIN.depth) / 2
	};

	var k = this.kitchen;
	var self = this;
	this.models = {};
	this.viewer.clear();
	var m = this.models;
	var mat = new M3.Material("#ca004e", "#FFF", 3, M3.LINEFILLEDQUAD, true, null);

	var c = new M3.Mesh();
	c.makeCube();
	c.setBakedMaterial(mat);
	this.viewer.addMesh(c);
	m.main = c;

	c = new M3.Arrow(0, -0.2, k.height + 0.1, k.width, -0.2, k.height + 0.1);
	c.addToScene(this.viewer);
	m.warrow = c;

	c = new M3.Arrow(k.width + 0.2, -0.10, 0, k.width + 0.2, -0.10, k.height, 0, 1, 0);
	c.addToScene(this.viewer);
	m.harrow = c;

	c = new M3.Arrow(k.width + 0.2, 0, -0.1, k.width + 0.2, k.depth, -0.1);
	c.addToScene(this.viewer);
	m.darrow = c;

	var updateScene = function  () {
		m.main.setScale(k.width, k.depth, k.height);
		m.warrow.set(0, -0.2, k.height + 0.1, k.width, -0.2, k.height + 0.1);
		m.harrow.set(k.width + 0.2, -0.10, 0, k.width + 0.2, -0.10, k.height, 0, 1, 0);
		m.darrow.set(k.width + 0.2, 0, -0.1, k.width + 0.2, k.depth, -0.1);
		self.viewer.setScale(5.0 / (k.width * 1.5));
		self.viewer.camera.setPos(k.width / 2, 0, 0);
		self.viewer.needUpdate = true;
	}

	var selectArrow = function (param) {
		m[param.charAt(0)+'arrow'].highlight();
		self.viewer.needUpdate = true;
	};

	var changeParam = function (param, value) {
		m[param.charAt(0)+'arrow'].gray();
		var v = Math.min(KCON.MAX[param], Math.max(value, KCON.MIN[param]));
		k[param] = v;
		updateScene();
		return Math.floor(v * 100);
	};

	updateScene();

	this.clearBalloons();

	var b = new KCON.Balloon();
	b.setMark('i');
	b.setXY(600, 100);
	b.addInput('Ширина: ', "width", selectArrow, changeParam, k.width);
	b.addInput('Высота: ', "height", selectArrow, changeParam, k.height);
	b.addInput('Глубина: ', "depth", selectArrow, changeParam, k.depth);
	b.addButton('Готово', function() {
		KCON.active.step3b();
	});
	this.addBalloon(b);
};

/**************************************************************************************************************************/
/**************************************************************************************************************************/
/* навесные ящики */
KCON.Constructor.prototype.step3a = function () {

	var k = this.kitchen;
	var self = this;
	//this.viewer.clear();
	var m = this.models;
	var mat = new M3.Material("#ca004e", "#FFF", 3, M3.LINEFILLEDQUAD, true, null);
	
	k.hdepth = k.hdepth || 0.5;//KCON.average('hdepth');
	k.hheight = k.hheight || 0.8;//KCON.average('hheight');
	k.hoffset = k.hoffset || (k.height * 0.5 + 1.2);

	// delete old arrows
	m.larrow.remove(this.viewer);
	m.rarrow.remove(this.viewer);
	m.darrow.remove(this.viewer);
	m.harrow.remove(this.viewer);

	var c = new M3.Mesh();
	c.makeCube();
	c.setBakedMaterial(mat);
	this.viewer.addMesh(c);
	m.hcorner = c;

	c = new M3.Mesh();
	c.makeCube();
	c.setBakedMaterial(mat);
	this.viewer.addMesh(c);
	m.hleft = c;

	c = new M3.Mesh();
	c.makeCube();
	c.setBakedMaterial(mat);
	this.viewer.addMesh(c);
	m.hright = c;

	c = new M3.Arrow(k.lwidth + k.depth + 0.2, 0, k.height + k.hoffset, k.lwidth + k.depth + 0.2, 0, k.height + k.hoffset + k.hheight, 0, 1, 0);
	c.addToScene(this.viewer);
	c.gray();
	m.hharrow = c;

	c = new M3.Arrow(0, k.rwidth + k.depth + 0.2, k.height + k.hoffset, k.depth, k.rwidth + k.depth + 0.2, k.height + k.hoffset);
	c.addToScene(this.viewer);
	c.gray();
	m.hdarrow = c;

	var updateScene = function () {
		m.hcorner.setScale(k.hdepth, k.hdepth, k.hheight);
		m.hcorner.setPos(0, 0, k.hoffset)
		m.hleft.setScale(k.lwidth + k.depth - k.hdepth, k.hdepth, k.hheight);
		m.hleft.setPos(k.hdepth, 0, k.hoffset);
		m.hright.setScale(k.hdepth, k.rwidth + k.depth - k.hdepth, k.hheight);
		m.hright.setPos(0, k.hdepth, k.hoffset);
		m.hharrow.set(k.lwidth + k.depth + 0.2, 0, k.hoffset, k.lwidth + k.depth + 0.2, 0, k.hoffset + k.hheight);
		m.hdarrow.set(0, k.rwidth + k.depth + 0.2, k.hoffset, k.depth, k.rwidth + k.depth + 0.2, k.hoffset);
		self.viewer.setScale(2.5 / Math.max(k.lwidth, k.rwidth));
		self.viewer.needUpdate = true;
	};

	var selectArrow = function (param) {
		m[param.charAt(0)+param.charAt(1)+'arrow'].highlight();
		self.viewer.needUpdate = true;
	};

	var changeParam = function (param, value) {
		m[param.charAt(0)+param.charAt(1)+'arrow'].gray();
		var v = Math.min(KCON.MAX[param], Math.max(value, KCON.MIN[param]));
		k[param] = v;
		updateScene();
		return Math.floor(v * 100);
	}

	updateScene();

	this.clearBalloons();

	var b = new KCON.Balloon();
	b.setMark('i');
	b.setXY(600, 100);
	b.addInput('Высота навесных ящиков: ', "hheight", selectArrow, changeParam, k.hheight);
	b.addInput('Глубина навесных ящиков: ', "hdepth", selectArrow, changeParam, k.hdepth);
	b.addButton('Готово', function() {
		KCON.active.step3a();
	});
	this.addBalloon(b);

}

KCON.Constructor.prototype.step3b = function() {
	var k = this.kitchen;
	var self = this;
	//this.viewer.clear();
	var m = this.models;
	var mat = new M3.Material("#ca004e", "#FFF", 3, M3.LINEFILLEDQUAD, true, null);
	
	k.hdepth = k.hdepth || 0.5;//KCON.average('hdepth');
	k.hheight = k.hheight || 0.8;//KCON.average('hheight');
	k.hoffset = k.hoffset || (k.height * 0.5 + 1.2);

	// delete old arrows
	m.warrow.remove(this.viewer);
	m.darrow.remove(this.viewer);
	m.harrow.remove(this.viewer);

	var c = new M3.Mesh();
	c.makeCube();
	c.setBakedMaterial(mat);
	this.viewer.addMesh(c);
	m.hang = c;

	c = new M3.Arrow(k.width + 0.2, -0.10, 0, k.width + 0.2, -0.10, k.height, 0, 1, 0);
	c.addToScene(this.viewer);
	m.hharrow = c;

	c = new M3.Arrow(k.width + 0.2, 0, -0.1, k.width + 0.2, k.depth, -0.1);
	c.addToScene(this.viewer);
	m.hdarrow = c;

	var updateScene = function  () {
		m.hang.setScale(k.width, k.depth, k.hheight);
		m.hang.setPos(0, 0, k.hoffset);
		m.hharrow.set(k.width + 0.2, -0.10, k.hoffset, k.width + 0.2, -0.10, k.hoffset + k.height, 0, 1, 0);
		m.hdarrow.set(k.width + 0.2, 0, k.hoffset - 0.1, k.width + 0.2, k.depth, k.hoffset - 0.1);
		self.viewer.setScale(5.0 / (k.width * 1.5));
		self.viewer.camera.setPos(k.width / 2, 0, 0);
		self.viewer.needUpdate = true;
	}

	var selectArrow = function (param) {
		m[param.charAt(0)+param.charAt(1)+'arrow'].highlight();
		self.viewer.needUpdate = true;
	};

	var changeParam = function (param, value) {
		m[param.charAt(0)+param.charAt(1)+'arrow'].gray();
		var v = Math.min(KCON.MAX[param], Math.max(value, KCON.MIN[param]));
		k[param] = v;
		updateScene();
		return Math.floor(v * 100);
	};

	updateScene();

	this.clearBalloons();

	var b = new KCON.Balloon();
	b.setMark('i');
	b.setXY(600, 100);
	b.addInput('Высота навесных ящиков: ', "hheight", selectArrow, changeParam, k.hheight);
	b.addInput('Глубина навесных ящиков: ', "hdepth", selectArrow, changeParam, k.hdepth);
	b.addButton('Готово', function() {
		KCON.active.step3a();
	});
	this.addBalloon(b);

};