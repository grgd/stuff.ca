
ymaps.ready(init);
var map;

function init(){     
    ymaps.geolocation.get().then(function (res) {
        // Предполагается, что на странице подключен jQuery
	    var bounds = res.geoObjects.get(0).properties.get('boundedBy'),
	        mapState = ymaps.util.bounds.getCenterAndZoom(
	            bounds,
	            [600, 600]
	        );
	     map = new ymaps.Map('main-map', mapState);
	});
}