
var fs = require('fs'),
	poisson = require('./poisson-sampling.js'),
	utils = require('./utils.js'),
	Room = utils.room,
	Door = utils.door,
	dirs = utils.dirs;

var rooms = [],
	doors = [];

var width = 40,
	height = 80;

var sampler = new poisson.sampler(width, height, 4, 10),
	solution = sampler.sampleUntilSolution();
var errors = 0;

for(var i=0; i<solution.length; i++) {
	var p = solution[i];
	p.x = p.x | 0;
	p.y = p.y | 0;
	if(p.x < width - 1 && p.x > 0 && p.y < height - 1 && p.y > 0)
		rooms.push(new Room(p.x, p.y, 0, 0));
	else
		errors ++;
}

function growRooms() {
	var growed = true;
	while(growed) {
		growed = false;
		for(var i=0; i<rooms.length; i++) {
			var r = rooms[i],
				d = Math.random() * dirs.length | 0;
			r.expand(d);
			var flag = true;
			if(r.x < 0 || r.y < 0 || r.x + r.w >= width || r.y + r.h >= height)
				flag = false;
			for(var j=0; j<rooms.length && flag; j++) {
				if(j != i && r.collide(rooms[j])) 
					flag = false;
			}
			if(flag) 
				growed = true;
			else
				r.contract(d);
		}
	}
}

function makeConnections() {
	for(var i=0; i<rooms.length; i++) {
		var r1 = rooms[i]
		for(var j=i+1; j<rooms.length; j++) {
			var r2 = rooms[j];
			
		}
	}
}

growRooms();
drawMap();

function drawMap() {
	var map = [];
	for(var i=0; i<width; i++) {
		map[i] = [];
		for(var j=0; j<height; j++)
			map[i][j] = 1;
	}
	for(var i=0; i<rooms.length; i++) {
		var r = rooms[i];
		for(var x = r.x; x < r.x + r.w; x++)
			for(var y = r.y; y < r.y + r.h; y++)
				map[x][y] = 0;
		for(var x = r.x; x < r.x + r.w; x++) {
			map[x][r.y] = 1;
			map[x][r.y + r.h] = 1;
		}
		for(var y = r.y; y < r.y + r.h; y++) {
			map[r.x][y] = 1;
			map[r.x + r.w][y] = 1;
		}
	}
	var s = "" + solution.length + " - " + errors + "\n";
	for(var i=0; i<width; i++) {
		for(var j=0; j<height; j++)
			s += map[i][j] == 1 ? "#" : ".";
		s += "\n";
	}
	fs.writeFileSync("map.txt", s);
}
