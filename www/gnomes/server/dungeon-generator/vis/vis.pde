
String ss[] = loadStrings("..\\map.txt");
println(ss.length - 1);
println(ss[1].length());
int h = ss.length - 1;
int w = ss[1].length();

int cellSize = 5;

size(w * cellSize, h * cellSize);
color cols[] = new color[2];
cols[0] = color(200, 200, 190);
cols[1] = color(34, 34, 34);
stroke(100, 100, 100);
for(int i=0; i<w; i++) {
 for(int j=0; j<h; j++) {
  char c = ss[j + 1].charAt(i);
  int t = 0;
  switch(c) {
   case '.': t = 0; break;
   case '#': t = 1; break; 
  }
  fill(cols[t]);
  rect(i * cellSize, j * cellSize, cellSize, cellSize);
 } 
}
