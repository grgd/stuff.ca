function Room (x, y, w, h) {
	this.x = x || 0;
	this.y = y || 0;
	this.w = w || 0;
	this.h = h || 0;

	this.doors = [];
}

Room.prototype.left = function() {
	return this.x;
};

Room.prototype.top = function() {
	return this.y;
};

Room.prototype.right = function() {
	return this.x + this.w;
};

Room.prototype.bottom = function() {
	return this.y + this.h;
};

Room.prototype.collide = function(a) {
	var l = this.left(),
		t = this.top(),
		r = this.right(),
		b = this.bottom();
	if(l >= a.right() || t >= a.bottom() || r <= a.left() || b <= a.top() )
		return false;
	return true;
};

Room.prototype.expand = function(dir) {
	switch(dir) {
		case 0: this.w++; break;
		case 1: this.h++; break;
		case 2: this.w++; this.x--; break;
		case 3: this.h++; this.y--; break;
	}
};

Room.prototype.contract = function(dir) {
	switch(dir) {
		case 0: this.w--; break;
		case 1: this.h--; break;
		case 2: this.w--; this.x++; break;
		case 3: this.h--; this.y++; break;
	}
};

function Door (a, b) {
	this.a = a || null;
	this.b = b || null;
}

var dirs = [{x: 1, y: 0}, {x: 0, y: 1}, {x: -1, y: 0}, {x: 0, y: -1}];

module.exports = {
	room: Room,
	door: Door,
	dirs: dirs
};