var io = require('socket.io').listen(800);

 var width = 21,
 	 height = 21,
 	 map = [],
 	 players = {};

 var dirs = [
	{x: -1, y: 0},
	{x: 0, y: 1},
	{x: 1, y: 0},
	{x: 0, y: -1}
];

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function generateColor () {
  var a = Math.random() * Math.PI * 0.5,
      r = (Math.sin(a) + 1) * 0.5,
      g = (Math.cos(a) + 1) * 0.5,
      b = Math.random();
  return rgbToHex( (r * 255) | 0, (g * 255) | 0, (b * 255) | 0 );
}

function generateMap () {
	for(var i=0; i<width; i++) {
		map[i] = [];
		for(var j=0; j<height; j++) {
			map[i][j] = i % 2 == 0 && j % 2 == 0 ? 1 : 0;
		}
	}
}

function Player(id) {
	this.id = id;
	this.x = ((Math.random() * (width - 1) / 2) | 0) * 2 + 1.5;
	this.y = ((Math.random() * (height - 1) / 2) | 0) * 2 + 1.5;
	this.dir = 0;
	this.targetX = this.x;
	this.targetY = this.y;
	this.speed = 5.0;
	this.radius = 8.0;
	this.color = generateColor();
}

generateMap();

io.sockets.on('connection', function (socket) {

  console.log("New user arrived");

  var p = new Player(socket.id);

  socket.emit('greetings', { info: p, others: players, map: map});

  socket.broadcast.emit('freshman', {info: p});

  players[socket.id] = p;

  console.log("User gained color " + players[socket.id].color);

  socket.on('disconnect', function () {
    console.log("User " + players[socket.id].color + " lost us");
    socket.broadcast.emit('left', {id: socket.id});
    delete players[socket.id];
  });

  socket.on('wannamove', function(data) {
  	var p = players[socket.id];
  	p.targetX += dirs[data.dir].x;
  	p.targetY += dirs[data.dir].y;
  	p.dir = data.dir;
  	console.log("Player " + p.color + " moved onto " + data.dir);
  	socket.broadcast.emit('movement', {
  		id: socket.id,
  		tx: p.targetX,
  		ty: p.targetY,
  		dir: p.dir
  	});
  	socket.emit('imovement', {
  		id: socket.id,
  		tx: p.targetX,
  		ty: p.targetY,
  		dir: p.dir
  	});
  });

});