
var Player = function(x, y, speed, color, radius, id, me) {
	this.x = x || Map.width() / 2;
	this.y = y || Map.height() / 2;
 
	this.speed = speed;

	this.id = id;

	this.targetX = this.x;
	this.targetY = this.y;

	this.moving = 0.0;
	this.requested = false;
	this.dir = 0;

	this.mesh = new THREE.Mesh(new THREE.SphereGeometry(0.4, 6, 6), new THREE.MeshBasicMaterial({color: color, shading: THREE.FlatShading}));
	this.mesh.position.set(this.x, this.y, 0.5);

	if(me) {
		this.light = new THREE.PointLight(0xAAAAAA, 1, radius);
		this.light.position.set(0, 0, 1.5);
		this.mesh.add(this.light);
	}

	this.canMove = function(dir) {
		if(this.moving > 0.4)
			return false;
		//this.dir = dir;
		var tx = Math.round(this.targetX + dirs[dir].x - 0.4),
			ty = Math.round(this.targetY + dirs[dir].y - 0.4);
		if(Map.cell(tx | 0, ty | 0) == 0) {
			return true;
		}
	}

	this.move = function(dir, tx, ty) {
		this.dir = dir;
		/*var tx = (this.targetX + dirs[dir].x),
			ty = (this.targetY + dirs[dir].y);*/
		this.targetX = tx;
		this.targetY = ty;
		this.step(0.0);
	}

	this.step = function(dt) {
		var dx = this.targetX - this.x,
			dy = this.targetY - this.y,
			d = dirs[this.dir];
		this.moving = Math.sqrt(dx * dx + dy * dy);
		if(this.moving < this.speed * dt) {
			this.x = this.targetX;
			this.y = this.targetY;
			return;
		}

		var prj = dx * d.x + dy * d.y;

		this.x += sign(prj * d.x) * this.speed * dt;
		this.y += sign(prj * d.y) * this.speed * dt;

		d = dirs[(this.dir + 1) % 4];
		prj = dx * d.x + dy * d.y;

		this.x += prj * d.x * this.speed * dt;
		this.y += prj * d.y * this.speed * dt;

		this.mesh.position.set(this.x, this.y, 0.5);
	}
}

var Game = (function() {
	var players = [],
		player = null,
		socket = io(ADDR),
		lastFrame = 0;

	socket.on('connect', function() {
		socket.on('greetings', function(data) {
			var p = data.info;
			player = new Player(p.x, p.y, p.speed, p.color, p.radius, p.id, true);
			players.push(player);
			for(var i in data.others) {
				var o = data.others[i];
				players.push(new Player(o.x, o.y, o.speed, o.color, o.radius, o.id));
			}
			Map.init(data.map);
			init();
		});

		socket.on('freshman', function(data) {
			var p = data.info,
				f = new Player(p.x, p.y, p.speed, p.color, p.radius, p.id);
			players.push(f);
			Display.addObject(f.mesh);
		});

		socket.on('movement', function(data) {
			for(var i=0; i<players.length; i++) {
				if(players[i].id == data.id) {
					players[i].move(data.dir, data.tx, data.ty);
				}
			}
		});

		socket.on('left', function(data) {
			var index = -1;
			for(var i=0; i<players.length; i++) {
				if(players[i].id == data.id) {
					index = i;
				}
			}
			if(index >= -1) {
				Display.removeObject(players[index].mesh);
				players.splice(index, 1);
			}
		});

		socket.on('imovement', function(data) {
			player.requested = false;
			player.move(data.dir, data.tx, data.ty);
		});
	});

	function init() {
		for(var i=0; i<players.length; i++) {
			Display.addObject(players[i].mesh);
		}
		Display.addObject(Map.mesh());
		Display.addObject(Map.shadow());
		Display.addObject(new THREE.AmbientLight(0x171615));

		Display.init();
		Input.init();

		lastFrame = +(new Date());
		step();
	}

	function step() {
		var newFrame = +(new Date()),
			dt = (newFrame - lastFrame) / 1000.0;
		lastFrame = newFrame;
		for(var i=0; i<players.length; i++) {
			players[i].step(dt);
		}
		Display.moveCamera(player.x, player.y);
		for(var i=0; i<4; i++)
			if(Input.keyDown(i) && !player.requested && player.canMove(i)) {
				//player.move(i);
				player.requested = true;
				socket.emit('wannamove', {dir: i});
			}
		requestAnimationFrame(step);
	}

})();