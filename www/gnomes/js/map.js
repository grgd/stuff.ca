
var Map = (function() {
	
	var mesh = null,
		shadow = null,
		map = [],
		width = 61,
		height = 61;

	function generate(info) {
		map = [];
		width = info.length;
		height = info[0].length;
		for(var i=0; i<width; i++) {
			map[i] = [];
			for(var j=0; j<height; j++) {
				map[i][j] = info[i][j];//Math.random() > 0.2 ? 0 : 1;
			}
		}
	}

	function createMesh() {
		var mg = new THREE.Geometry(),
			sg = new THREE.Geometry();

		var nodes = {},
			vertices = [],
			mfaces = [],
			sfaces = [],
			c = 0;
		for(var i=0; i<=width; i++) {
			for(var j=0; j<=height; j++) {
				var v1 = new THREE.Vector3(i, j, 0);
				nodes[i + "-" + j + "-0"] = c;
				c++;
				vertices.push(v1);
				var v2 = new THREE.Vector3(i, j, 1);
				nodes[i + "-" + j + "-1"] = c;
				c++;
				vertices.push(v2);
				var v3 = new THREE.Vector3(i, j, 15);
				nodes[i + "-" + j + "-2"] = c;
				c++;
				vertices.push(v3);
			}
		}

		function addf(i1, i2, i3, i4, i5, i6, i7, i8, i9, bottom, top) {
			if(top) {
				sfaces.push(new THREE.Face3(
					nodes[i1 + "-" + i2 + "-" + (i3+1)],
					nodes[i4 + "-" + i5 + "-" + (i6+1)],
					nodes[i7 + "-" + i8 + "-" + (i9+1)]
				));
			}
			if(bottom) {
				mfaces.push(new THREE.Face3(
						nodes[i1 + "-" + i2 + "-" + i3],
						nodes[i4 + "-" + i5 + "-" + i6],
						nodes[i7 + "-" + i8 + "-" + i9]
					));
			}
		}

		for(var i=0; i<width; i++) 
			for(var j=0; j<height; j++) {
				if(map[i][j] == 0) {
					addf(i, j, 0,
						 i+1, j, 0,
						 i+1, j+1, 0, true);
					addf(i, j, 0,
						 i+1, j+1, 0,
						 i, j+1, 0, true);
					if(i + 1 == width || map[i+1][j] == 1) {
						addf(i+1, j, 0,
							 i+1, j, 1,
							 i+1, j+1, 1, true, true);
						addf(i+1, j, 0,
							 i+1, j+1, 1,
							 i+1, j+1, 0, true, true);
					}
					if(i == 0 || map[i - 1][j] == 1) {
						addf(i, j, 0,
							 i, j+1, 1,
							 i, j, 1, true, true);
						addf(i, j, 0,
							 i, j+1, 0,
							 i, j+1, 1, true, true);
					}
					if(j + 1 == height || map[i][j+1] == 1) {
						addf(i+1, j+1, 0,
							 i+1, j+1, 1,
							 i, j+1, 1, true, true);
						addf(i+1, j+1, 0,
							 i, j+1, 1,
							 i, j+1, 0, true, true);
					}
					if(j == 0 || map[i][j-1] == 1) {
						addf(i, j, 0,
							 i, j, 1,
							 i+1, j, 1, true, true);
						addf(i, j, 0,
							 i+1, j, 1,
							 i+1, j, 0, true, true);
					}
				} else {
					addf(i, j, 1,
						 i+1, j, 1,
						 i+1, j+1, 1, false, true);
					addf(i, j, 1,
						 i+1, j+1, 1,
						 i, j+1, 1, false, true);
				}
			}
		mg.vertices = vertices;
		mg.faces = mfaces;
		mg.computeFaceNormals();
		mesh = new THREE.Mesh(mg, new THREE.MeshPhongMaterial(0xFFFFFF));
		sg.vertices = vertices;
		sg.faces = sfaces;
		sg.computeFaceNormals();
		shadow = new THREE.Mesh(sg, new THREE.MeshBasicMaterial({color: 0x171615, shading: THREE.FlatShading}));
	}

	function init(info) {
		generate(info);
		createMesh();
	}

	return {
		init: init,
		mesh: function() { return mesh; },
		shadow: function() { return shadow; },
		width: function() { return width; },
		height: function() { return height; },
		cell: function(x, y) { 
				if(x < 0 || x >= width || y < 0 || y >= height)
					return 1;
				else
					return map[x][y];
			}
	}
})();