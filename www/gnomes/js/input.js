
var dirs = [
	{x: -1, y: 0},
	{x: 0, y: 1},
	{x: 1, y: 0},
	{x: 0, y: -1}
];

var Input = (function() {
	var keys = [false, false, false, false];

	function init() {
		window.addEventListener('keydown', function(event) {
			if(event.keyCode >= 37 && event.keyCode <= 40)
				keys[event.keyCode - 37] = true;
		}, false);

		window.addEventListener('keyup', function(event) {
			if(event.keyCode >= 37 && event.keyCode <= 40)
				keys[event.keyCode - 37] = false;
		}, false);
	}

	return {
		keyDown: function(key) { return keys[key]; },
		init: init
	}
})();