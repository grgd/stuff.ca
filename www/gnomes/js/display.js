
var Display = (function() {
	var canvas = document.getElementById("mainCanvas"),
		width,
		height,
		screenSize;

	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera( 75, 1.0, 0.1, 1000 );

	var renderer = new THREE.WebGLRenderer({canvas: canvas});
	renderer.shadowMapEnabled = true;

	function resize () {
		width = window.innerWidth;
		height = window.innerHeight;

		screenSize = width > height ? height : width;
		canvas.width = screenSize;
		canvas.height = screenSize;
		renderer.setSize( screenSize, screenSize);
	}

	resize();
	window.addEventListener('resize', resize);

	renderer.setSize( screenSize, screenSize);

	scene.add(camera);
	camera.position.z = 10;

	function addObject(ob) {
		scene.add(ob);
	}

	function removeObject(ob) {
		scene.remove(ob);
	}

	function renderLoop() {
		renderer.render(scene, camera);
		requestAnimationFrame(renderLoop);
	}

	function moveCamera(x, y) {
		camera.position.x = x;
		camera.position.y = y;
	}

	function init() {
		renderLoop();
	}

	return {
		init: init,
		camera: camera,
		addObject: addObject,
		removeObject: removeObject,
		moveCamera: moveCamera,
		screenSize: function() { return screenSize; }
	};
})();