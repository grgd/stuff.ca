﻿function calcAround(i,j) {
	var k = 0;
	var s = 0;
	for(k=0; k<8; k++) {
		if(map[i+dirs[k][0]][j+dirs[k][1]]==WALL)
			s++;
	}
	return s;
}

function generate() {
	//cave();
	room();
	
	var i = 0;
	for(i=0; i<size; i++) {
		map[i][0] = WALL;
		map[i][size-1] = WALL;
		map[0][i] = WALL;
		map[size-1][i] = WALL;
	}
			
}

function room() {
	var i = 0;
	var j = 0;
	for(i=0; i<size; i++) {
			for(j=0; j<size; j++) {
				map[i][j] = FLOOR;
			}
		}
	for(i=0; i<size; i++) {
		map[i][0] = WALL;
		map[i][size-1] = WALL;
		map[0][i] = WALL;
		map[size-1][i] = WALL;
	}
	roomz(0,0,size,size);
}

function roomz(left,top,wid,hei) {	
	var i=0;
	var j=0;
	var k=0;

	if(wid > hei && wid > 5 && hei > 4) {
		k = Math.floor(Math.random()*(wid*0.4)+wid*0.3);
		for(i=0; i<hei; i++)
			map[left+k][top+i] = WALL;
		roomz(left,top,k,hei);
		roomz(left+k,top,wid-k,hei);
		
		for(i=0; i<hei/10; i++) {	
			j = Math.floor(Math.random()*(hei-2)+1);
			if(Math.random() > 0.6)
				map[left+k][top+j] = DOOR1;
			else
				map[left+k][top+j] = FLOOR;
			map[left+k+1][top+j] = FLOOR;
			map[left+k-1][top+j] = FLOOR;
		}
	} 
	if(wid <= hei && hei > 5 && wid > 4) {
		k = Math.floor(Math.random()*(hei*0.4)+hei*0.3);
		for(i=0; i<wid; i++)
			map[left+i][top+k] = WALL;
		roomz(left,top,wid,k);
		roomz(left,top+k,wid,hei-k);
		
		for(i=0; i<wid/10; i++) {		
			j = Math.floor(Math.random()*(wid-2)+1);
			if(Math.random() > 0.6)
				map[left+j][top+k] = DOOR1;
			else
				map[left+j][top+k] = FLOOR;
			map[left+j][top+k+1] = FLOOR;
			map[left+j][top+k-1] = FLOOR;
		}
	}
}

function cellIterate() {
	for(i=1; i<size-1; i++) 
		for(j=1; j<size-1; j++) {
			if(map[i][j] == WALL && calcAround(i,j)<2)
				map[i][j] = FLOOR;
			else if(map[i][j] == FLOOR && calcAround(i,j)>5)
				map[i][j] = WALL;
		}
}

function cave() {
	var i = 0;
	var j = 0;
	var k = 0
	for(i=0; i<size; i++) {
		for(j=0; j<size; j++) {
			map[i][j] = WALL;
		}
	}
	
	for(k=0; k<size*size*0.6; k++) {
		i = Math.floor(Math.random()*(size-3))+2;
		j = Math.floor(Math.random()*(size-3))+2;
		map[i][j] = FLOOR;
	}
	
	for(k=0; k<4; k++)
		cellIterate();
}