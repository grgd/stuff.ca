﻿
var map = [];
var size = 46;

var monsters = [];
	
var mask = []; // Маска видимости тайлов на экране
for(var i=0; i<size; i++) {
	mask[i] = [];
	for(var j=0; j<size; j++) {
		mask[i][j] = 0;
	}
}

var FLOOR = 0;
var WALL = 1;
var DOOR2 = 2;
var DOOR1 = 3;

var dirs = [[1,0],[0,1],[-1,0],[0,-1],[1,1],[-1,1],[-1,-1],[1,-1]];

var hero = {
	x: 1,
	y: 1,
	hp: 15,
	inv: ["Топор", "Шапка"]
};

function init() {
	if(-[1,]) { //not IE
		initGraphics();
	} else {
		initGraphics();
		initGraphics_ex();
	}
	document.onkeydown = keyHook;
	updateInv();
	prepareMap();
	update();
}

function repaint() {
	var c = document.getElementById("mCanvas").getContext("2d");
	c.fillStyle = "red";
	c.fillRect(0,0,320,320);
}

function keyHook(e) {
	switch(event.keyCode) {
		case 37: moveHero(2); break;
		case 38: moveHero(3); break;
		case 39: moveHero(0); break;
		case 40: moveHero(1); break;
	}
}

function updateInv() {
	var c = document.getElementById("inventory");
	var items = c.childNodes;
	
	while(items.length)
		c.removeChild(items[0]);
	
	var i = 0;
	for(i=0; i<hero.inv.length; i++) {
		var item = document.createElement("li");
		item.innerText = hero.inv[i];
		c.appendChild(item);
	}
}

function moveHero(dir) {
	var nx = hero.x + dirs[dir][0];
	var ny = hero.y + dirs[dir][1];
	if(map[nx][ny]%2 == 0) {
		hero.x = nx;
		hero.y = ny;
	} else {
		if(map[nx][ny]%32 == DOOR1)
			map[nx][ny]--;
	}
	update();
}

function prepareMap() {
	var i = 0;
	var j = 0;
	for(i=0; i<size; i++) {
		map[i] = [];
		for(j=0; j<size; j++)
			if(j%2==0 && i%2==0)
				map[i][j] = WALL;
			else
				map[i][j] = FLOOR;
	}
	generate();
}

function update() {
	buildMask();
	buildLocal();
	if(-[1,]) { //not IE
		drawMap();
	} else {
		drawMap_ex();
	}
}

var cx = 0;
var cy = 0;
function explore(i,j) {
	cx = hero.x-scrHSize+i;
	cy = hero.y-scrHSize+j;
	mask[i][j] = 1+map[cx][cy]%2;
	map[cx][cy] = map[cx][cy] | 32;
}

//////////////////////////////////////////////////////////////////////
// THE NEARBY PLACE //
//////////////////////////////////////////////////////////////////////
function buildLocal() {
	var i,ic;
	var j,jc;
	for(i=0; i<scrSize; i++) {
		ic = i+hero.x-scrHSize;
		if(ic>=0 && ic<size) {
			for(j=0; j<scrSize; j++) {
				jc = j+hero.y-scrHSize;
				if(jc>=0 && jc<size) {
				/*******************************/
					if(map[ic][jc] >= 32) { // explored
						loc[i][j].tile = map[ic][jc]%32;
						if(mask[i][j] > 0) // if visible
							loc[i][j].over = -1; // clear cell
						else
							loc[i][j].over = 32; // drop shadow
					} else { // if unexplored
						loc[i][j].tile = 32;
						loc[i][j].over = -1;
					}
				/*******************************/
				} else {
					loc[i][j].tile = 32;
					loc[i][j].over = -1;
				}
			}
		} else {
			for(j=0; j<scrSize; j++) {
				loc[i][j].tile = 32;
				loc[i][j].over = -1;
			}
		}
	}
	loc[scrHSize][scrHSize].over = 0;
}

//////////////////////////////////////////////////////////////////////
// THE MASK //
//////////////////////////////////////////////////////////////////////
function buildMask() {
	var i = 0;
	var j = 0;
	
	for(i=0; i<size; i++)
		for(j=0; j<size; j++)
			mask[i][j] = 0;
	//mask[scrHSize][scrHSize] = 1;
	explore(scrHSize,scrHSize);
	
	for(i=1; i<scrHSize && map[hero.x+i][hero.y]%2==0; i++)
		explore(scrHSize+i,scrHSize)
	for(i=1; i<scrHSize && map[hero.x-i][hero.y]%2==0; i++)
		explore(scrHSize-i,scrHSize);
	for(i=1; i<scrHSize && map[hero.x][hero.y+i]%2==0; i++)
		explore(scrHSize,scrHSize+i);
	for(i=1; i<scrHSize && map[hero.x][hero.y-i]%2==0; i++)
		explore(scrHSize,scrHSize-i);
	for(i=1; i<scrHSize; i++) {
		traceHor(hero.x+scrHSize, hero.y+i, 1, 1);
		traceHor(hero.x+scrHSize, hero.y+i, 1, -1);
		traceHor(hero.x+scrHSize, hero.y+i, -1, 1);
		traceHor(hero.x+scrHSize, hero.y+i, -1, -1);
		
		traceVer(hero.x+i, hero.y+scrHSize, 1, 1);
		traceVer(hero.x+i, hero.y+scrHSize, 1, -1);
		traceVer(hero.x+i, hero.y+scrHSize, -1, 1);
		traceVer(hero.x+i, hero.y+scrHSize, -1, -1);
	}	
	
	var k = 0;
	var x = 0;
	var y = 0;
	for(i=0; i<size; i++)
		for(j=0; j<size; j++) {
			if(mask[i][j] == 1) {
					x = hero.x-scrHSize+i;
					y = hero.y-scrHSize+j;
					if(x>0 && y>0 && x<size-1 && y<size-1)
						for(k=0; k<8; k++)
							if(map[x+dirs[k][0]][y+dirs[k][1]]%2==1) {
								/*if(map[x+dirs[k][0]][y+dirs[k][1]] < 32)
									map[x+dirs[k][0]][y+dirs[k][1]] += 32;*/
								explore(i+dirs[k][0],j+dirs[k][1]);
							}
			}
		}
	
}

function traceHor(x,y,invX,invY) {	
	var dx = Math.abs(hero.x-x);
	var dy = Math.abs(hero.y-y);
	var err = 0;
	var de = dy;
	var i = 1;
	var j = 1;
	
	while(i<scrHSize && map[hero.x+i*invX][hero.y+j*invY]%2 == 0) {
		explore(scrHSize+i*invX,scrHSize+j*invY);
		err += de;
		if(2*err >= dx) {
			j++;
			err -= dx;
		}
		i++;
	}
}

function traceVer(x,y,invX,invY) {	
	var dx = Math.abs(hero.x-x);
	var dy = Math.abs(hero.y-y);
	var err = 0;
	var de = dx;
	var i = 1;
	var j = 1;
	
	while(j<scrHSize && map[hero.x+i*invX][hero.y+j*invY]%2 == 0) {
		explore(scrHSize+i*invX,scrHSize+j*invY);
		err += de;
		if(2*err >= dy) {
			i++;
			err -= dy;
		}
		j++;
	}
}
