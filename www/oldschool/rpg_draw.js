﻿
var tile = 16;
var scrSize = 20;
var scrHSize = 10;
var tiles = [];
var scr = [];
var loc = [];

	tiles[0] = new Image();
	tiles[0].src = "tiles/floor1.png";
	tiles[1] = new Image();
	tiles[1].src = "tiles/wall1.png";
	tiles[2] = new Image();
	tiles[2].src = "tiles/door2.png";
	tiles[3] = new Image();
	tiles[3].src = "tiles/door1.png";
	tiles[32] = new Image();
	tiles[32].src = "tiles/shadow.png";
	
var critters = [];

	critters[0] = new Image();
	critters[0].src = "tiles/batman2.png";
	critters[1] = new Image();
	critters[1].src = "tiles/bad1.png";
	critters[32] = new Image();
	critters[32].src = "tiles/shadow.png";
	
function initGraphics() {
	var i = 0;
	var j = 0;
	
	for(i=0; i<scrSize; i++) {
		loc[i] = [];
		for(j=0; j<scrSize; j++) {
			loc[i][j] = {
				tile: 32,
				over: 32,
				critter: -1,
				item: -1
			}
		}
	}
}

function initGraphics_ex() {
	var c = document.getElementById("screen");
	var items = c.childNodes;
	
	while(items.length)
		c.removeChild(items[0]);
		
	var start = new Date();
	
	var i=0;
	var j=0;
	for(i=0; i<scrSize; i++) {
		scr[i] = [];
		for(j=0; j<scrSize; j++) {
			var d = document.createElement("div");
			d.className = "tile";
			d.style.background = "url("+tiles[0].src+")";
			var im = document.createElement("img");
			d.appendChild(im);
			im.style.display = "none";
			scr[i][j] = d;
			c.appendChild(d);
		}
	}
	var d = document.createElement("img");
	d.src = critters[0].src;
	scr[scrHSize][scrHSize].appendChild(d);
	
	var end = new Date();
	document.title = 'Скорость ' + (end.getTime()-start.getTime()) + ' мс';
}

	
function drawPerson(who,x,y) {
	var c = document.getElementById("mCanvas").getContext("2d");
	c.drawImage(critters[who], x*tile, y*tile);
}

function drawMap() {
	var c = document.getElementById("mCanvas").getContext("2d");
	c.fillStyle = "black";
	c.fillRect(0,0,tile*scrSize,tile*scrSize);
	
	var start = new Date();
	var i,ic;
	var j,jc;
	for(i=0; i<scrSize; i++) {
		ic = i+hero.x-scrHSize;
		if(ic>=0 && ic<size)
			for(j=0; j<scrSize; j++) {
				jc = j+hero.y-scrHSize;
				if(jc>=0 && jc<size) {
					if(loc[i][j].tile >= 0) {
						c.drawImage(tiles[loc[i][j].tile],i*tile,j*tile);
						if(loc[i][j].over >= 0)
						if(loc[i][j].over >= 0)
							c.drawImage(critters[loc[i][j].over],i*tile,j*tile);
					}
				}
			}
	}
	
	var end = new Date();
	document.title = 'Скорость ' + (end.getTime()-start.getTime()) + ' мс';
}

function setTile(x, y, t) {
	if(t==32) {
		scr[y][x].childNodes[0].style.display = "none";
		//scr[y][x].style.background = "none";
	} //else
	scr[y][x].style.background = "url("+tiles[t].src+")";
}

function setImg(x, y, i) {
	if(i < 0) {
		scr[y][x].childNodes[0].style.display = "none";
	} else {
		scr[y][x].childNodes[0].style.display = "block";
		scr[y][x].childNodes[0].src = critters[i].src;
	}
}

function drawMap_ex() {

	var start = new Date();
	
	var i,ic;
	var j,jc;
	for(i=0; i<scrSize; i++) {
		for(j=0; j<scrSize; j++) {
			setTile(i, j, loc[i][j].tile);
			setImg(i, j, loc[i][j].over );
		}
	}
	
	var end = new Date();
	document.title = 'Скорость ' + (end.getTime()-start.getTime()) + ' мс';
}