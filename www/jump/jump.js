
var LEAP = 100,
	JUMP = LEAP / 4,
	DIFFICULTY = 0.8;

var ctx = document.getElementById('canvas').getContext('2d');

var buf = [];

function jump(x) {
	return -x * (x - LEAP) / LEAP;
}

function jump0(x) {
	var t = LEAP / 2;
	return -(x - t) * (x + t) / LEAP;
}

function sqEq(a, b, c) {
	var d = b * b - 4 * a * c;
	if(d < 0) {
		console.log("Oh shit " + d);
		return false;
	}
	d = Math.sqrt(d);
	var x1 = (-b - d) / (2 * a);
	var x2 = (-b + d) / (2 * a);
	return [x1, x2];
}

function Obstacle (x, width, height) {

	this.x = x;
	this.width = width;

	if(height == undefined) {
		var h = jump0(width / 2);
		this.height = h * 0.9;
	} else
		this.height = height;

	this.range = [0,0,0,0];
	this.update();
}

Obstacle.prototype.update = function() {
	var hw = this.width / 2,
		target = this.height;

	var anchor = sqEq(-1.0 / LEAP, 0, LEAP / 4.0 - target);

	if(anchor == false) {
		console.log("Wrong Obstacle (" + this.width + ", " + this.height + ")");
		return;
	}
	//console.log("Good Obstacle (" + this.width + ", " + this.height + ")", anchor);

	anchor[1] = -(LEAP / 2 - anchor[0] + hw);
	anchor[0] = 0;

	//console.log(anchor);

	var p1 = anchor[1];
		p3 = p1 + LEAP;

	this.range = [
		-p3,
		p1,
		-p1,
		p3
	];

	buf.push((this.range[3] - this.range[2]) / LEAP);
	console.log("Diff: " + buf[buf.length-1]);
};

Obstacle.prototype.move = function(dx) {
	this.x += dx;
};

Obstacle.prototype.getRange = function() {
	return [
		this.x + this.range[0],
		this.x + this.range[1],
		this.x + this.range[2],
		this.x + this.range[3]
	];
};


Obstacle.prototype.draw = function() {
	ctx.fillStyle = "#343434";
	ctx.fillRect(this.x - this.width / 2, 300 - this.height, this.width, this.height);

	var r = this.getRange();
	ctx.strokeStyle = "red";
	ctx.beginPath();
	ctx.moveTo(r[0], 300);
	ctx.quadraticCurveTo((r[0] + r[2]) / 2, 300 - JUMP * 2, r[2], 300);
	ctx.stroke();

	ctx.beginPath();
	ctx.moveTo(r[1], 300);
	ctx.quadraticCurveTo((r[1] + r[3]) / 2, 300 - JUMP * 2, r[3], 300);
	ctx.stroke();
};


ctx.fillStyle = "#fff";
ctx.fillRect(0, 0, 1800, 300);

var obs = [];
for(var i=0; i<7; i++) {
	//var o = new Obstacle((i+1) * 1000 / 5, 0.5 * LEAP);
	var o = new Obstacle(200 * (i + 1), (i + 1) * LEAP / 8.0);
	obs.push(o);
}

for(var i=0; i<obs.length; i++) {
	obs[i].draw();
}
