(function() {
  var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                              window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || (function(f){window.setTimeout(f, 0);});
  window.requestAnimationFrame = requestAnimationFrame;
})();

var FPSCounter = function (outputField) {
	var fps = 0;
	var self = this;
	this.collect = function () {
		if(outputField == document)
			outputField.title = fps.toString() + ' fps';
		else
			outputField.innerHTML = fps.toString() + ' fps';
		fps = 0;
		setTimeout(self.collect, 1000);
	}

	this.addFrame = function() {
		fps++;
	}

	this.collect();

	return this;
};

var CB = CB || {};

CB.BoidGrid = function BoidGrid (cellSize) {
	this.canvas = document.createElement('canvas');
	this.canvas.width = (window.innerWidth / cellSize) | 0;
	this.canvas.height = (window.innerHeight / cellSize) | 0;
	/*this.canvas.style.width = "100%";
	this.canvas.style.height = "100%";*/
	var t = "scale3d(" + (window.innerWidth / this.canvas.width).toString() + "," + (window.innerHeight / this.canvas.height).toString() + ",1)";
	this.canvas.style.webkitTransform = t;
	this.canvas.style.transform = t;
	document.body.appendChild(this.canvas);

	this.cellSize = cellSize;
	this.width = this.canvas.width;//Math.floor(this.canvas.width / cellSize) | 0;
	this.height = this.canvas.height;//Math.floor(this.canvas.height / cellSize) | 0;

	var boids = [];
	for (var i = 0; i < this.width * this.height; i++) {
		boids[i] = new CB.Boid(128, 128, 128, (i % this.width) * 1.0 / this.width, Math.floor(i / this.height) * 1.0 / this.height );
	};
	var w = this.width,
		h = this.height,
		k,
		dirs = [[1,0],[0,1],[-1,0],[0,-1]];
	for(var i = 0; i < this.width; i++) {
		for(var j = 0; j < this.height; j++) {
			k = i + j * w;
			for(var l = 0; l < dirs.length; l++)
				boids[k].neighbors.push(boids[(i + dirs[l][0] + w) % w + ((j + dirs[l][1] + h) % h) * w]);
		}
	}
	this.boids = boids;
	this.frame = 0;
}

CB.BoidGrid.prototype.step = function step() {
	this.frame++;
	for (var i = this.boids.length - 1; i >= 0; i--) {
		this.boids[i].step(0.5, 15, 20.0);
	};
	for (var i = this.boids.length - 1; i >= 0; i--) {
		this.boids[i].update();
	};
}

CB.BoidGrid.prototype.draw = function draw() {
	//this.canvas.width = this.canvas.width;
	var ctx = this.canvas.getContext('2d'),
		imgdata = ctx.getImageData(0, 0, this.width, this.height);
		data = imgdata.data;
	var i, l, c;
	l = this.boids.length;
	for (i = l - 1; i >= 0; i--) {
		c = this.boids[i].color;
		data[i * 4] = c.r | 0;
		data[i * 4 + 1] = c.g | 0;
		data[i * 4 + 2] = c.b | 0;
		data[i * 4 + 3] = 255;
	};
	ctx.putImageData(imgdata, 0, 0);
	/*for(i = 0; i < this.width; i++) {
		for(j = 0; j < this.height; j++) {
			ctx.fillStyle = this.boids[i + j * this.width].color.toString();
			ctx.fillRect(i * this.cellSize, j * this.cellSize, this.cellSize, this.cellSize);
		}
	}*/
};

CB.Color = function Color(r, g, b) {
	this.r = r || 0;
	this.g = g || 0;
	this.b = b || 0;
};

CB.Color.prototype.add = function(c) {
	this.r += c.r;
	this.g += c.g;
	this.b += c.b;
}

CB.Color.prototype.sub = function(c) {
	this.r -= c.r;
	this.g -= c.g;
	this.b -= c.b;
}

CB.Color.prototype.zero = function() {
	this.r = 0;
	this.g = 0;
	this.b = 0;
};

CB.Color.prototype.iadd = function(c) {
	var r = this.r + c.r,
		g = this.g + c.g,
		b = this.b + c.b;
	return new CB.Color(r, g, b);
}

CB.Color.prototype.isub = function(c) {
	var r = this.r - c.r,
		g = this.g - c.g,
		b = this.b - c.b;
	return new CB.Color(r, g, b);
}

CB.Color.prototype.setAdd = function(a, b) {
	this.r = a.r + b.r;
	this.g = a.g + b.g;
	this.b = a.b + b.b;
}

CB.Color.prototype.setSub = function(a, b) {
	this.r = a.r - b.r;
	this.g = a.g - b.g;
	this.b = a.b - b.b;
}

CB.Color.prototype.scale = function(a) {
	this.r *= a;
	this.g *= a;
	this.b *= a;
}

CB.Color.prototype.mag = function() {
	return Math.sqrt(this.r * this.r + this.g * this.g + this.b * this.b + 0.001);
}

CB.Color.prototype.sqrMag = function() {
	return this.r * this.r + this.g * this.g + this.b * this.b;
}

CB.Color.prototype.norm = function() {
	var l = 1.001 / Math.sqrt(this.r * this.r + this.g * this.g + this.b * this.b + 0.001);
	this.r *= l;
	this.g *= l;
	this.b *= l;
}

CB.Color.prototype.clone = function() {
	return new CB.Color(this.r, this.g, this.b);
}

CB.Color.prototype.copyTo = function(c) {
	c.r = this.r;
	c.g = this.g;
	c.b = this.b;
}

CB.Color.prototype.shuffle = function(mag) {
	this.r = mag * (Math.random() - 0.5);
	this.g = mag * (Math.random() - 0.5);
	this.b = mag * (Math.random() - 0.5);
}

CB.Color.prototype.bounce = function(v) {
	if(this.r > 255) {
		this.r = 255;
		v.r *= -1;
	}
	if(this.r < 0) {
		this.r = 0;
		v.r *= -1;
	}
	if(this.g > 255) {
		this.g = 255;
		v.g *= -1;
	}
	if(this.g < 0) {
		this.g = 0;
		v.g *= -1;
	}
	if(this.b > 255) {
		this.b = 255;
		v.b *= -1;
	}
	if(this.b < 0) {
		this.b = 0;
		v.b *= -1;
	}
}

CB.temp = {
	sep: new CB.Color(),
	aveC: new CB.Color(),
	aveV: new CB.Color(),
	d: new CB.Color()
};

CB.Boid = function Boid (initR, initG, initB, x, y) {
	// position in 3d color space
	this.color = new CB.Color(initR, initG, initB);

	// and a buffer value
	this.bcolor = this.color.clone();

	// velocity vector
	this.v = new CB.Color(0, 0, 0);

	// buffered velocity
	this.bv = this.v.clone();

	// actual position on a 2d grid (0..1)
	this.x = x;
	this.y = y;

	// links to the neighbors
	this.neighbors = [];
}

CB.Boid.prototype.step = function step(ease, dist, hate) {
	var sep = CB.temp.sep,//new CB.Color(),
		aveC = CB.temp.aveC, //new CB.Color(),
		aveV = CB.temp.aveV, //new CB.Color(),
		d = CB.temp.d, //new CB.Color(),
		b,
		l;
	aveC.zero();
	aveV.zero();
	sep.shuffle(0.1);
	for(var i = this.neighbors.length-1; i >= 0; i--) {
		b = this.neighbors[i];

		aveC.add(b.color);

		aveV.add(b.v);

		d.setSub(this.color, b.color);
		if(d.sqrMag() < dist) {
			sep.add(d);
		}
	}

	l = 1.0 / this.neighbors.length;
	aveC.scale(l);
	aveV.scale(l);

	sep.norm();
	sep.scale(hate);

	sep.r = ease * (sep.r + aveC.r + aveV.r - this.color.r - this.v.r);
	sep.g = ease * (sep.g + aveC.g + aveV.g - this.color.g - this.v.g);
	sep.b = ease * (sep.b + aveC.b + aveV.b - this.color.b - this.v.b);
	/*sep.add(aveV);
	sep.add(aveC);
	sep.sub(this.color);
	sep.sub(this.bv);
	sep.scale(ease);*/

	this.bv.add(sep);
	//this.bv.scale(0.95);
	this.bcolor.add(this.bv);

	this.bcolor.bounce(this.bv);
}

CB.Boid.prototype.update = function() {
	this.bcolor.copyTo(this.color);
	this.bv.copyTo(this.v);
}

CB.Color.prototype.toString = function() {
	/*var s = '#';
	if(this.r > 15)
		s += (this.r | 0).toString(16);
	else
		s += '0' + (this.r | 0).toString(16);

	if(this.g > 15)
		s += (this.g | 0).toString(16);
	else
		s += '0' + (this.g | 0).toString(16);

	if(this.b > 15)
		s += (this.b | 0).toString(16);
	else
		s += '0' + (this.b | 0).toString(16);*/


	return "#"+this.LUT[this.r | 0]+this.LUT[this.g | 0]+this.LUT[this.b | 0];
}

CB.Color.prototype.LUT = [
	"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0a", "0b", "0c", "0d", "0e", "0f", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1a", "1b", "1c", "1d", "1e", "1f", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "2a", "2b", "2c", "2d", "2e", "2f", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "3a", "3b", "3c", "3d", "3e", "3f", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4a", "4b", "4c", "4d", "4e", "4f", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5a", "5b", "5c", "5d", "5e", "5f", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b", "6c", "6d", "6e", "6f", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7a", "7b", "7c", "7d", "7e", "7f", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8a", "8b", "8c", "8d", "8e", "8f", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9a", "9b", "9c", "9d", "9e", "9f", "a0", "a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "aa", "ab", "ac", "ad", "ae", "af", "b0", "b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "ba", "bb", "bc", "bd", "be", "bf", "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "ca", "cb", "cc", "cd", "ce", "cf", "d0", "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "da", "db", "dc", "dd", "de", "df", "e0", "e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "ea", "eb", "ec", "ed", "ee", "ef", "f0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "fa", "fb", "fc", "fd", "fe", "ff"
];