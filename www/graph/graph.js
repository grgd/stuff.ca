
(function() {
	var WIDTH = 500,
		HEIGHT = 500,
		W2 = WIDTH / 2,
		H2 = HEIGHT / 2,
		XSCALE = 5.0,
		YSCALE = 5.0,
		map = [];

	var canvas = document.getElementById('main-canvas'),
		input = document.getElementById('main-formula'),
		button = document.getElementById('main-button');

	function makeFunction(val) {
		var func = null;
		var bootstrap = "var rnd = Math.random, tan = Math.tan, pow = Math.pow,atan = Math.atan,sqrt = Math.sqrt,sin = Math.sin,cos = Math.cos,max = Math.max,min = Math.min,abs = Math.abs; try { return (%CODE%); } catch(err) { alert('Ошибка при вычислении. Может быть в формулу закралась опечатка?'); } return null";
		try {
			func = new Function("x,y,r,phi", bootstrap.replace('%CODE%', val));
		} catch(err) {
			alert("Ошибка в формуле. Может быть есть незакрытая скобка?");
		}
		return func;
	}

	function replot(func) {
		var ctx = canvas.getContext('2d'),
			imgData = ctx.getImageData(0, 0, WIDTH, HEIGHT),
			data = imgData.data;

		var x, y, r, phi;
		var mx, mn, t, d, m;
		t = func(Math.random() * 1e-3, Math.random() * 1e-3, Math.random() * 1e-3, Math.random() * 1e-3);
		if(!isFinite(t)) t = 0;
		mx = mn = t;
		m = 0;
		for(var j=0; j<HEIGHT; j++) {
			y = (j - H2 + 0.5) * YSCALE / H2;
			for(var i=0; i<WIDTH; i++) {
				x = (i - W2 + 0.5) * XSCALE / W2;
				r = Math.sqrt(x * x + y * y);
				phi = Math.atan2(y, x);
				t = func(x, y, r, phi);
				if(t === null)
					return;
				if(!isFinite(t)) 
					t = 0;
				if(t > mx) mx = t;
				if(t < mn) mn = t;
				map[i][j] = t;
				m += t;
			}			
		}
		m = m / (WIDTH * HEIGHT);
		d = 0;
		for(var i=0; i<WIDTH; i++) {
			for(var j=0; j<HEIGHT; j++) {
				d += Math.pow(map[i][j] - m, 2);
			}
		}
		d = Math.sqrt(d / (WIDTH * HEIGHT)) * 100;
		mx = mx - mn;
		if(mx > d) {
			mn = m - d / 2;
			mx = d;
		}
		console.log("D: " + d);
		console.log("Min: " + mn);
		console.log("Max: " + (mn + mx));
		var k;
		for(var i=0; i<WIDTH; i++) {
			for(var j=0; j<HEIGHT; j++) {
				t = (map[i][j] - mn) / mx;
				if(!isFinite(t))
					t = 0.0;
				t = t * 255 | 0;
				k = (j * WIDTH + i) * 4;
				data[k] = t;
				data[k + 1] = t;
				data[k + 2] = t;
				data[k + 3] = 255;
			}
		}

		ctx.putImageData(imgData, 0, 0);
	}

	var busy = false;
	function refresh() {
		if(busy)
			return;
		busy = true;
		button.innerText = "Едем...";
		var f = makeFunction(input.value);
		if(f) {
			setTimeout(function() {
				replot(f);
				busy = false;
				button.innerText = "Поехали!";
			}, 0);
		}
	}

	for(var i=0; i<WIDTH; i++) {
		map[i] = [];
		for(var j=0; j<HEIGHT; j++) {
			map[i][j] = 0.0;
		}
	}

	button.addEventListener('click', refresh);

	input.addEventListener('keyup', function(event) {
		if(event.keyCode == 13) {
			refresh();
		}
	});

	replot(makeFunction("sqrt(rnd())"));

	var example = [
		"pow(2, -r * r * sin(phi * 5))",
		"sin(phi + sin(r * 10))",
		"cos(phi * 4 + r * 10 + (cos(x * 10) + cos(y * 10)))",
		"cos(phi * 4 + sin(r * 5) + (cos(x * 20) + cos(y * 20)))",
		"cos(phi * 50 + 1000 / (r + x * x * 0.5))",
		"sin(5*x + 2 * sin(y*3) * pow(1.1, -r*r))",
		"cos(8 * phi * r + sin(x * 3))",
		"sin(x * 10) * sin(y * 10 + cos(r * 10) * sin(x))",
		"sin(x * x * 2 + 5 * cos(y)) * sin(y * y * 2 + 5 * cos(x))",
		"(sin(6*x + 4*y) + sin(4*x - 6*y)) *sin(r * r)",
		"cos(8 * phi * r + sin(x * 3) * pow(1.3, x))",
		"sin( r * r * 100) * cos(r * r * 50)",
		"sin(r * r * 100)",
		"cos(r * 50 + 2 * sin(phi * 50))",
		"cos(r * 20 * sin(r * 3) + 2 * sin(phi * 20))",
		"phi * phi + sin(r*10)"
	];
	input.value = example[Math.random() * example.length | 0];
})();