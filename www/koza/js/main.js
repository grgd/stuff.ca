
var template = new Image(),
	canvas = document.getElementById('main-canvas'),
	context = canvas.getContext('2d'),
	textarea = document.getElementById('main-textarea'),
	content = "",
	saveButton = document.getElementById('save-button');

var ANCHOR_X = 774,
	ANCHOR_Y = 74;

var templates = ["blank.jpg", "blank2.jpg"],
	templateNumber = -1;

context.font = 'normal 64px Lobster';
context.fillStyle = "#49a1c4";

template.onload = function () {
	redraw();
}

var blockFlag = false;

function unblockAndDraw() {
	blockFlag = false;
	content = textarea.value;
	redraw();
}

function keyHandler(event) {
	if(!blockFlag) {
		blockFlag = true;
		setTimeout(unblockAndDraw, 1000);
	}
}

textarea.addEventListener('keyup', keyHandler);

function redraw() {
	context.drawImage(template, 0, 0);
	context.wrapText(content, ANCHOR_X, ANCHOR_Y, 500, 64);
}

CanvasRenderingContext2D.prototype.wrapText = function (text, x, y, maxWidth, lineHeight) {

    var lines = text.split("\n");

    for (var i = 0; i < lines.length; i++) {

        var words = lines[i].split(' ');
        var line = '';

        for (var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + ' ';
            var metrics = this.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > maxWidth && n > 0) {
                this.fillText(line, x, y);
                line = words[n] + ' ';
                y += lineHeight;
            }
            else {
                line = testLine;
            }
        }

        this.fillText(line, x, y);
        y += lineHeight;
    }
}

var saving = false;
function save() {
	if(saving)
		return;
	saving = true;
	var data = canvas.toDataURL('image/png');
	data = data.replace(/^data:image\/png/,'data:application/octet-stream');
	saveButton.setAttribute('download', 'Koza-' + (Math.random() * 16536 | 0).toString(16) + '.jpg');
	saveButton.setAttribute('href', data);
	saving = false;
}

saveButton.addEventListener('click', save);

function nextTemplate() {
	templateNumber ++;
	templateNumber = templateNumber % templates.length;
	template.src = templates[templateNumber];
}

document.getElementById('change-button').addEventListener('click', nextTemplate);

nextTemplate();