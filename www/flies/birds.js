
;(function () {
	'use strict';

	var canvas = document.createElement("canvas"),
		width = document.documentElement.clientWidth-2,
		height = document.documentElement.clientHeight-2,
		halfW = width / 2,
		halfH = height / 2,
		tile = 200,
		nx = (width / tile) | 0,
		ny = (height / tile) | 0,
		/* distance to the edge of screen, where boids start turning away */
		border = 150,
		/* repelling force */
		borderForce = 0.00001;
	canvas.width = width;
	canvas.height = height;
	var ctx = canvas.getContext("2d");

	document.body.appendChild(canvas);
	var FPS = 0;
	var collectFPS = function function_name () {
		document.title = 'fps: ' + FPS.toString();
		FPS = 0;
	}
	setInterval(collectFPS, 1000);

	var map = [];
	for (var i = 0; i < nx; i++) {
		map[i] = [];
		for (var j = 0; j < ny; j++) {
			map[i][j] = null;
		};
	};

	var boids = [],
		count = (width * height / 7000.0) | 0;

	var gDist = new Vec2(0, 0),
		gMag = 0.0,
		i = 0,
		j = 0,
		time = 0;

	var sq = function (x) {
		return x * x;
	}

	var Boid = function Boid (x, y) {
		this.pos = new Vec2(x, y);
		var a = Math.random() * 2 * 3.1415926;
		this.vel = new Vec2(2 * Math.cos(a), 2 * Math.sin(a));

		this.t = new Vec2((x / tile) | 0, (y / tile) | 0);

		this.avePos = new Vec2(0, 0);
		this.cohension = 1.0;
		this.aveVel = new Vec2(0, 0);
		this.alignment = 2.0;
		this.sepVel = new Vec2(0, 0);
		this.separation = 1.5;
		this.hunger = 1.0;
		this.life = Math.random() * Math.PI * 4;
		this.neighbors = 0;

		this.mass = 0.004;//(Math.random() + 1) * 0.01;
		this.radius = 10 + Math.random() * 10;
		this.sqrRadius = this.radius * this.radius;
		this.sight = 30 + Math.random() * 20;
		this.sqrSight = this.sight * this.sight;

		this.next = null; // for a linked list
	}

	Boid.prototype.interact = function(b) {
		gDist.setSub(this.pos, b.pos);
		gMag = gDist.sqrMag();
		if(gMag < this.sqrSight) {
			this.avePos.add(b.pos);
			if(gDist.dot(this.vel) < 0)
				this.aveVel.add(b.vel);
			this.neighbors ++;

			/*b.avePos.add(this.pos);
			b.aveVel.add(this.vel);
			b.neighbors ++;*/

			if(gMag < this.sqrRadius) {
				gDist.scale(this.sqrRadius / (gMag + 1.0));
				this.sepVel.add(gDist);
				/*b.sepVel.sub(gDist);*/
			}
		}
	};

	Boid.prototype.step = function() {
		if(this.neighbors > 0) {
			this.avePos.scale(1.0 / this.neighbors);
			this.aveVel.scale(1.0 / this.neighbors);
			this.vel.x += this.mass * (this.alignment * (this.aveVel.x - this.vel.x) + this.cohension * (this.avePos.x - this.pos.x) + this.separation * this.sepVel.x);
			this.vel.y += this.mass * (this.alignment * (this.aveVel.y - this.vel.y) + this.cohension * (this.avePos.y - this.pos.y) + this.separation * this.sepVel.y);
		}
		//this.vel.tilt(0.01 * (Math.random() - 0.5));
		if(this.pos.x < border) {
			this.vel.x += sq(this.pos.x - border) * borderForce;
		}
		if(this.pos.y < border) {
			this.vel.y += sq(this.pos.y - border) * borderForce;
		}
		if(this.pos.x > width - border) {
			this.vel.x -= sq(this.pos.x - width + border) * borderForce;
		}
		if(this.pos.y > height - border) {
			this.vel.y -= sq(this.pos.y - height + border) * borderForce;
		}
		this.vel.norm(1.5);
		this.pos.add(this.vel);
		this.avePos.set(0, 0);
		this.aveVel.set(0, 0);
		this.sepVel.set(0, 0);
		this.neighbors = 0;
		this.life += 0.1;

		if(this.pos.x < 0) {
			this.pos.x = 0;
			this.vel.x *= -1;
		}
		if(this.pos.y < 0) {
			this.pos.y = 0;
			this.vel.y *= -1;
		}
		if(this.pos.x > width) {
			this.pos.x = width;
			this.vel.x *= -1;
		}
		if(this.pos.y > height) {
			this.pos.y = height;
			this.vel.y *= -1;
		}
	};

	for(i=0; i<count; i++) {
		boids.push(new Boid(Math.random() * width, Math.random() * height));
	}

	var step = function () {
		var b;
		for(i=0; i<count; i++) {
			b = boids[i];
			for(j=0; j<count; j++) 
				if(i != j)
					b.interact(boids[j]);
		}

		ctx.fillStyle = "#FFF";
		ctx.fillRect(0, 0, width, height);
		ctx.fillStyle = "#00F";

		for(i=0; i<count; i++) {
			b = boids[i];
			b.step();
			ctx.fillRect((b.pos.x - 5) | 0, (b.pos.y - 5) | 0, 10, 10);
		}
		FPS++;
		time++;
		requestAnimationFrame(step);
	}	

	step();	
}());